Steps:
----

1. Add a client on PowerShell using the below command
Add-AdfsClient -ClientId "vro_bpm_oauth_app" -Name "vro_bpm_oauth_app" -RedirectUri @("http://localhost:8083", "http://localhost:8083/login") -Description "OAuth 2.0 client for vro_bpm_oauth_app"

2. Verify it was added using the below command
Get-AdfsClient -ClientId "vro_bpm_oauth_app"

3. Add a relying party from ADFS admin console, with the following properties
Relying Party Name: vro_bpm_oauth_app
Identifier: https://vro_bpm_oauth_app.mazeedad.co/adfs/services/trust

4. This is the user authentication URL
https://192.168.1.50/adfs/oauth2/authorize?resource=https://vro_bpm_oauth_app.mazeedad.co/adfs/services/trust&client_id=vro_bpm_oauth_app&redirect_uri=http://localhost:8083/login&response_type=code&state=8lfkw7
<!doctype html>
<html>
	<head>
		<meta name="viewport" content="width=device-width" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>First Delay Notification</title>
		<style>
		</style>
	</head>
	<body class="">
		Dear <b>${mandateBO.businessOwnerUsername}</b>,
		
		This is an automated message from VRO BPM System.
		
		This is the first delay notification, related to the following mandate. Please take an action before the next escalation notification.
		
		Mandate Details:
			Subject: ${mandateBO.subject}
			Incoming Number: ${mandateBO.incomingNumber}
			Outgoing Number: ${mandateBO.outgoingNumber}
			Duration: ${mandateBO.duration}
			
		Thanks,
		VRO BPM System
  	</body>
</html>
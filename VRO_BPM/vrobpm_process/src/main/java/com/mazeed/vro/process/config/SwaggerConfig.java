package com.mazeed.vro.process.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.classmate.TypeResolver;

import io.swagger.annotations.ApiModelProperty;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	@Autowired
	private TypeResolver typeResolver;

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).additionalModels(typeResolver.resolve(ProcessVariable.class))
			.host("http://localhost:8080")
			.select()
			.apis(RequestHandlerSelectors.basePackage("com.mazeed.vro.process.controller")).paths(PathSelectors.any()).build().apiInfo(apiEndPointsInfo())
			.tags(new Tag("processinstances", "API for managing proccess instances"), new Tag("tasks", "API for managing tasks"));
	}

	private ApiInfo apiEndPointsInfo() {
		return new ApiInfoBuilder().title("ProcessEngine REST API").description("ProcessEngine REST API").contact(new Contact("Islam Abdo", "", "iabdo@mazeed.co")).version("1.0").build();
	}

	// Helper class for Swagger documentation
	public static class ProcessVariable {
		@ApiModelProperty(example="402881d26b750000016b754559760001")
		private String mandateId;

		public String getMandateId() {
			return mandateId;
		}

		public void setMandateId(String mandateId) {
			this.mandateId = mandateId;
		}
	}
}
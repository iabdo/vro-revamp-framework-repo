package com.mazeed.vro.process.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.rest.dto.task.TaskDto;
import org.camunda.bpm.engine.task.IdentityLinkType;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.task.TaskQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.mazeed.vro.process.enumeration.UserAction;
import com.mazeed.vro.process.util.TaskUtilities;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;

@RestController
@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
@Api(tags = { "tasks" })
public class TaskController {
	@Autowired
	RuntimeService runtimeService;

	@Autowired
	TaskService taskService;

	// This lists all tasks with/ without a condition; This should be used by admins
	@ApiOperation(value = "View a list of all tasks with/ without a condition", response = TaskDto.class, produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list"), @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
		@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"), @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
		@ApiResponse(code = 500, message = "Internal Error") })
	@RequestMapping(path = "/tasks", method = RequestMethod.GET, produces = "application/json")
	@PreAuthorize("hasAuthority('ADMIN')")
	@Authorization(value = "oauth2")
	public ResponseEntity<?> listTasks(Authentication authentication,
		@ApiParam(name = "Authorization", value = "The bearer token value", required = true) @RequestHeader(name = "Authorization", required = true) String bearerToken,

		@ApiParam(name = "processDefinitionKey", required = false) @RequestParam(value = "processDefinitionKey", required = false) String processDefinitionKey,

		@ApiParam(name = "processInstanceId", required = false) @RequestParam(value = "processInstanceId", required = false) String processInstanceId,

		@ApiParam(name = "assignee", required = false) @RequestParam(value = "assignee", required = false) String assignee,

		@ApiParam(name = "owner", required = false) @RequestParam(value = "owner", required = false) String owner,

		@ApiParam(name = "candidateGroup", required = false) @RequestParam(value = "candidateGroup", required = false) String candidateGroup,

		@ApiParam(name = "candidateUser", required = false) @RequestParam(value = "candidateUser", required = false) String candidateUser) {

		// Create the task query
		TaskQuery taskQuery = taskService.createTaskQuery();

		taskQuery = taskQuery.active();

		if (processDefinitionKey != null)
			taskQuery = taskQuery.processDefinitionKey(processDefinitionKey);

		if (processInstanceId != null)
			taskQuery = taskQuery.processInstanceId(processInstanceId);

		if (assignee != null)
			taskQuery = taskQuery.taskAssignee(assignee);

		if (owner != null)
			taskQuery = taskQuery.taskOwner(owner);

		if (candidateGroup != null)
			taskQuery = taskQuery.taskCandidateGroup(candidateGroup);

		if (candidateUser != null)
			taskQuery = taskQuery.taskCandidateUser(candidateUser);

		List<Task> tasks = taskQuery.list();

		// Convert to DTO
		Gson gson = new GsonBuilder().create();
		String data = gson.toJson(tasks);

		java.lang.reflect.Type typeOfSrc = new TypeToken<List<TaskDto>>() {
		}.getType();
		List<TaskDto> taskDTOs = gson.fromJson(data, typeOfSrc);
		return new ResponseEntity<>(taskDTOs, HttpStatus.OK);
	}

	// This lists all tasks for the currently logged-in user
	@ApiOperation(value = "View a list of all tasks for the currently logged-in user", response = TaskDto.class, produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list"), @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
		@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"), @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
		@ApiResponse(code = 500, message = "Internal Error") })
	@RequestMapping(path = "/tasks", method = RequestMethod.POST, produces = "application/json")
	@PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
	@Authorization(value = "oauth2")
	public ResponseEntity<?> listTasksForCurrentUser(Authentication authentication,
		@ApiParam(name = "Authorization", value = "The bearer token value", required = true) @RequestHeader(name = "Authorization", required = true) String bearerToken) {
		
		// Create the task query
		List<Task> allTasks = new ArrayList<Task>();
		TaskQuery taskQuery = taskService.createTaskQuery();

		if (authentication != null) {
			allTasks.addAll(taskQuery.taskAssignee(authentication.getName()).list());
			allTasks.addAll(taskQuery.taskOwner(authentication.getName()).list());

			Set<String> authorities = AuthorityUtils.authorityListToSet(authentication.getAuthorities());
			List<String> candidateGroups = new ArrayList<String>(authorities);
			if (candidateGroups != null && !candidateGroups.isEmpty())
				allTasks.addAll(taskQuery.taskCandidateGroupIn(candidateGroups).list());
		}

		allTasks = allTasks.stream().distinct().collect(Collectors.toList());

		// Convert to DTO
		Gson gson = new GsonBuilder().create();
		String data = gson.toJson(allTasks);

		java.lang.reflect.Type typeOfSrc = new TypeToken<List<TaskDto>>() {
		}.getType();
		List<TaskDto> taskDTOs = gson.fromJson(data, typeOfSrc);
		return new ResponseEntity<>(taskDTOs, HttpStatus.OK);
	}

	// This claims the task for the currently logged-in user
	@ApiOperation(value = "Claim the task with specific Id for the currently logged-in user", produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 204, message = "Successfully claimed task"), @ApiResponse(code = 401, message = "You are not authorized to claim the task"),
		@ApiResponse(code = 403, message = "Claiming the task with the provided Id is forbidden"), @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
		@ApiResponse(code = 500, message = "Internal Error, or task Id doesn't exist") })
	@RequestMapping(path = "/tasks/{taskId:.*}/claim", method = RequestMethod.POST, produces = "application/json")
	@PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
	@Authorization(value = "oauth2")
	public ResponseEntity<?> claimTaskForCurrentUser(Authentication authentication, @PathVariable String taskId,
		@ApiParam(name = "Authorization", value = "The bearer token value", required = true) @RequestHeader(name = "Authorization", required = true) String bearerToken) {

		boolean isAuthorizedToComplete = TaskUtilities.isUserAuthorizedOnTask(taskId, authentication.getName(), authentication.getAuthorities(), taskService, UserAction.CLAIM);

		if (!isAuthorizedToComplete) {
			throw new AccessDeniedException("The user is not authorized to claim the task");
		}

		// Claim the task
		taskService.claim(taskId, authentication.getName());

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	// This un-claims the task for the currently logged-in user
	@ApiOperation(value = "Unclaim the task with specific Id for the currently logged-in user", produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 204, message = "Successfully unclaimed task"), @ApiResponse(code = 401, message = "You are not authorized to unclaim the task"),
		@ApiResponse(code = 403, message = "Claiming the task with the provided Id is forbidden"), @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
		@ApiResponse(code = 500, message = "Internal Error, or task Id doesn't exist") })
	@RequestMapping(path = "/tasks/{taskId:.*}/unclaim", method = RequestMethod.POST, produces = "application/json")
	@PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
	@Authorization(value = "oauth2")
	public ResponseEntity<?> unclaimTaskForCurrentUser(Authentication authentication, @PathVariable String taskId,
		@ApiParam(name = "Authorization", value = "The bearer token value", required = true) @RequestHeader(name = "Authorization", required = true) String bearerToken) {

		boolean isAuthorizedToComplete = TaskUtilities.isUserAuthorizedOnTask(taskId, authentication.getName(), authentication.getAuthorities(), taskService, UserAction.UNCLAIM);

		if (!isAuthorizedToComplete) {
			throw new AccessDeniedException("The user is not authorized to unclaim the task");
		}

		taskService.deleteUserIdentityLink(taskId, authentication.getName(), IdentityLinkType.ASSIGNEE);
		taskService.deleteUserIdentityLink(taskId, authentication.getName(), IdentityLinkType.OWNER);

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	// This completes the task for the currently logged-in user
	@ApiOperation(value = "Complete the task with specific Id for the currently logged-in user", produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 204, message = "Successfully completed task"), @ApiResponse(code = 401, message = "You are not authorized to complete the task"),
		@ApiResponse(code = 403, message = "Completing the task with the provided Id is forbidden"), @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
		@ApiResponse(code = 500, message = "Internal Error, or task Id doesn't exist") })
	@RequestMapping(path = "/tasks/{taskId:.*}/complete", method = RequestMethod.POST, produces = "application/json")
	@PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
	@Authorization(value = "oauth2")
	public ResponseEntity<?> completeTaskForCurrentUser(Authentication authentication, @PathVariable String taskId, @RequestBody Map<String, Object> taskVariables,
		@ApiParam(name = "Authorization", value = "The bearer token value", required = true) @RequestHeader(name = "Authorization", required = true) String bearerToken) {

		boolean isAuthorizedToComplete = TaskUtilities.isUserAuthorizedOnTask(taskId, authentication.getName(), authentication.getAuthorities(), taskService, UserAction.COMPLETE);

		if (!isAuthorizedToComplete) {
			throw new AccessDeniedException("The user is not authorized to complete the task");
		}

		// Complete the task
		taskService.complete(taskId, taskVariables);

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	// This delegates the task for the currently logged-in user
	@ApiOperation(value = "Delegate the task with a specific Id for another user", produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 204, message = "Successfully delegated task"), @ApiResponse(code = 401, message = "You are not authorized to delegate the task"),
		@ApiResponse(code = 403, message = "Delegating the task with the provided Id is forbidden"), @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
		@ApiResponse(code = 500, message = "Internal Error, or task Id doesn't exist") })
	@RequestMapping(path = "/tasks/{taskId:.*}/delegate", method = RequestMethod.POST, produces = "application/json")
	@PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
	@Authorization(value = "oauth2")
	public ResponseEntity<?> delegateTaskToAnotherUser(Authentication authentication, @PathVariable String taskId, @RequestBody String userId,
		@ApiParam(name = "Authorization", value = "The bearer token value", required = true) @RequestHeader(name = "Authorization", required = true) String bearerToken) {

		boolean isAuthorizedToComplete = TaskUtilities.isUserAuthorizedOnTask(taskId, authentication.getName(), authentication.getAuthorities(), taskService, UserAction.DELEGATE);

		if (!isAuthorizedToComplete) {
			throw new AccessDeniedException("The user is not authorized to delegate the task");
		}

		// Delegate the task
		taskService.delegateTask(taskId, userId);

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	// This resolves the delegated task
	@ApiOperation(value = "Resolve the delegated task", produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 204, message = "Successfully resolved delegated task"), @ApiResponse(code = 401, message = "You are not authorized to resolve the task"),
		@ApiResponse(code = 403, message = "Resolving the delegated task with the provided Id is forbidden"), @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
		@ApiResponse(code = 500, message = "Internal Error, or task Id doesn't exist") })
	@RequestMapping(path = "/tasks/{taskId:.*}/resolve", method = RequestMethod.POST, produces = "application/json")
	@PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
	@Authorization(value = "oauth2")
	public ResponseEntity<?> resolveDelegatedTask(Authentication authentication, @PathVariable String taskId, @RequestBody Map<String, Object> taskVariables,
		@ApiParam(name = "Authorization", value = "The bearer token value", required = true) @RequestHeader(name = "Authorization", required = true) String bearerToken) {

		boolean isAuthorizedToComplete = TaskUtilities.isUserAuthorizedOnTask(taskId, authentication.getName(), authentication.getAuthorities(), taskService, UserAction.RESOLVE);

		if (!isAuthorizedToComplete) {
			throw new AccessDeniedException("The user is not authorized to resolve the task");
		}

		// Resolve the delegated task
		taskService.resolveTask(taskId, taskVariables);

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
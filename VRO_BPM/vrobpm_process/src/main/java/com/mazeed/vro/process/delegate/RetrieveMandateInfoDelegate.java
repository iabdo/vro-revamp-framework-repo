package com.mazeed.vro.process.delegate;

import java.util.Arrays;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mazeed.vro.process.dto.MandateDTO;
import com.mazeed.vro.process.util.RESTUtilities;

@Service
public class RetrieveMandateInfoDelegate implements JavaDelegate {

	@Value("${env.endpoint.vro-api}")
	String apiBaseUrl;

	public void execute(DelegateExecution execution) throws Exception {

		ObjectMapper mapper = new ObjectMapper();

		String mandateId = execution.getVariable("mandateId") != null 
							? execution.getVariable("mandateId").toString()
							: "";


		// Retrieve the mandate data
		String token = execution.getVariable("bearer_token") != null ? execution.getVariable("bearer_token").toString() : "";
		
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("Authorization", String.format("Bearer %s", token));
        
		ResponseEntity<MandateDTO> restResponse = RESTUtilities.callREST(apiBaseUrl + "mandates/" + mandateId,
				HttpMethod.GET, MediaType.APPLICATION_JSON, new ParameterizedTypeReference<MandateDTO>() {}, headers, null);
		MandateDTO mandateInfo = restResponse.getBody();

		//Variable mandateJson to be used by camunda embedded forms for displaying data
		execution.setVariable("mandateJson", mapper.writeValueAsString(mandateInfo));
		//Variable updatedMandateJson to be used by camunda embedded forms for updating data
		execution.setVariable("updatedMandateJson", "");
		
		
		execution.setVariable("mandateBO", mandateInfo);
	}

}

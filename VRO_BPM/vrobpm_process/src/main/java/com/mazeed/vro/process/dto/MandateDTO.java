package com.mazeed.vro.process.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MandateDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 596458862359830343L;

	@JsonProperty("id")
	private String id;

	@JsonProperty("incomingNumber")
	private String incomingNumber;

	@JsonProperty("originalIncomingNumber")
	private String originalIncomingNumber;

	@JsonProperty("outgoingNumber")
	private String outgoingNumber;

	@JsonProperty("subject")
	private String subject;

	@JsonProperty("startDate")
	private Date startDate;

	@JsonProperty("endDate")
	private Date endDate;

	@JsonProperty("duration")
	private int duration;

	@JsonProperty("source")
	private LookupDTO source;

	@JsonProperty("confidentialityLevel")
	private LookupDTO confidentialityLevel;

	@JsonProperty("mandateConsultantUsername")
	private String mandateConsultantUsername;

	@JsonProperty("directedBy")
	private String directedBy;

	@JsonProperty("completionStatus")
	private LookupDTO completionStatus;

	@JsonProperty("executionStatus")
	private LookupDTO executionStatus;

	@JsonProperty("fullSubject")
	private String fullSubject;

	@JsonProperty("generalNotes")
	private String generalNotes;

	@JsonProperty("wallet")
	private LookupDTO wallet;

	@JsonProperty("ministerPriority")
	private LookupDTO ministerPriority;

	@JsonProperty("ministerPriorityNotes")
	private String ministerPriorityNotes;

	@JsonProperty("ministerDirection")
	private LookupDTO ministerDirection;

	@JsonProperty("ministerDirectionNotes")
	private String ministerDirectionNotes;

	@JsonProperty("businessOwnerUsername")
	private String businessOwnerUsername;
	
	@JsonProperty("businessOwnerEmail")
	private String businessOwnerEmail;

	@JsonProperty("needsExtension")
	private boolean needsExtension;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getMandateConsultantUsername() {
		return mandateConsultantUsername;
	}

	public void setMandateConsultantUsername(String mandateConsultantUsername) {
		this.mandateConsultantUsername = mandateConsultantUsername;
	}

	public String getDirectedBy() {
		return directedBy;
	}

	public void setDirectedBy(String directedBy) {
		this.directedBy = directedBy;
	}

	public String getFullSubject() {
		return fullSubject;
	}

	public void setFullSubject(String fullSubject) {
		this.fullSubject = fullSubject;
	}

	public String getGeneralNotes() {
		return generalNotes;
	}

	public void setGeneralNotes(String generalNotes) {
		this.generalNotes = generalNotes;
	}

	public String getMinisterDirectionNotes() {
		return ministerDirectionNotes;
	}

	public void setMinisterDirectionNotes(String ministerDirectionNotes) {
		this.ministerDirectionNotes = ministerDirectionNotes;
	}

	public String getBusinessOwnerUsername() {
		return businessOwnerUsername;
	}

	public void setBusinessOwnerUsername(String businessOwnerUsername) {
		this.businessOwnerUsername = businessOwnerUsername;
	}

	public String getBusinessOwnerEmail() {
		return businessOwnerEmail;
	}

	public void setBusinessOwnerEmail(String businessOwnerEmail) {
		this.businessOwnerEmail = businessOwnerEmail;
	}

	public boolean isNeedsExtension() {
		return needsExtension;
	}

	public void setNeedsExtension(boolean needsExtension) {
		this.needsExtension = needsExtension;
	}

	public String getIncomingNumber() {
		return incomingNumber;
	}

	public void setIncomingNumber(String incomingNumber) {
		this.incomingNumber = incomingNumber;
	}

	public String getOriginalIncomingNumber() {
		return originalIncomingNumber;
	}

	public void setOriginalIncomingNumber(String originalIncomingNumber) {
		this.originalIncomingNumber = originalIncomingNumber;
	}

	public String getOutgoingNumber() {
		return outgoingNumber;
	}

	public void setOutgoingNumber(String outgoingNumber) {
		this.outgoingNumber = outgoingNumber;
	}

	public LookupDTO getWallet() {
		return wallet;
	}

	public void setWallet(LookupDTO wallet) {
		this.wallet = wallet;
	}

	public LookupDTO getMinisterPriority() {
		return ministerPriority;
	}

	public void setMinisterPriority(LookupDTO ministerPriority) {
		this.ministerPriority = ministerPriority;
	}

	public String getMinisterPriorityNotes() {
		return ministerPriorityNotes;
	}

	public void setMinisterPriorityNotes(String ministerPriorityNotes) {
		this.ministerPriorityNotes = ministerPriorityNotes;
	}

	public LookupDTO getMinisterDirection() {
		return ministerDirection;
	}

	public void setMinisterDirection(LookupDTO ministerDirection) {
		this.ministerDirection = ministerDirection;
	}
	
	public LookupDTO getSource() {
		return source;
	}

	public void setSource(LookupDTO source) {
		this.source = source;
	}

	public LookupDTO getConfidentialityLevel() {
		return confidentialityLevel;
	}

	public void setConfidentialityLevel(LookupDTO confidentialityLevel) {
		this.confidentialityLevel = confidentialityLevel;
	}

	public LookupDTO getCompletionStatus() {
		return completionStatus;
	}

	public void setCompletionStatus(LookupDTO completionStatus) {
		this.completionStatus = completionStatus;
	}

	public LookupDTO getExecutionStatus() {
		return executionStatus;
	}

	public void setExecutionStatus(LookupDTO executionStatus) {
		this.executionStatus = executionStatus;
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof MandateDTO)) {
			return false;
		}
		MandateDTO other = (MandateDTO) obj;
		return getId().equals(other.getId());
	}
}

package com.mazeed.vro.process.dto;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LookupDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7027520279611904753L;

	@JsonProperty("lookupPK")
	private LookupDTOPK lookupPK;

	@JsonProperty("lookupValue")
	private String lookupValue;

	@JsonProperty("lookupValueAr")
	private String lookupValueAr;

	public LookupDTO() {
		super();
	}

	public LookupDTO(String lookupType, String lookupKey) {
		super();
		this.lookupPK = new LookupDTOPK(lookupType, lookupKey);
	}

	public LookupDTO(String lookupType, String lookupKey, String lookupValue, String lookupValueAr) {
		super();
		this.lookupPK = new LookupDTOPK(lookupType, lookupKey);
		this.lookupValue = lookupValue;
		this.lookupValueAr = lookupValueAr;
	}

	public LookupDTOPK getLookupPK() {
		return lookupPK;
	}

	public void setLookupPK(LookupDTOPK lookupPK) {
		this.lookupPK = lookupPK;
	}

	public String getLookupValue() {
		return lookupValue;
	}

	public void setLookupValue(String lookupValue) {
		this.lookupValue = lookupValue;
	}

	public String getLookupValueAr() {
		return lookupValueAr;
	}

	public void setLookupValueAr(String lookupValueAr) {
		this.lookupValueAr = lookupValueAr;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof LookupDTO))
			return false;
		LookupDTO that = (LookupDTO) o;
		return Objects.equals(getLookupPK(), that.getLookupPK());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getLookupPK());
	}
}

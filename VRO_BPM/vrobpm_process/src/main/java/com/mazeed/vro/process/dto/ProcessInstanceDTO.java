package com.mazeed.vro.process.dto;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProcessInstanceDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8510232239915422622L;

	/**
	 * The unique identifier of the execution.
	 */
	@JsonProperty("id")
	private String id;
	/**
	 * Indicates if the execution is ended.
	 */
	@JsonProperty("ended")
	private boolean ended;
	/**
	 * Id of the root of the execution tree representing the process instance. It is
	 * the same as {@link #getId()} if this execution is the process instance.
	 */
	@JsonProperty("processInstanceId")
	private String processInstanceId;
	/**
	 * The id of the tenant this execution belongs to. Can be <code>null</code> if
	 * the execution belongs to no single tenant.
	 */
	@JsonProperty("tenantId")
	private String tenantId;
	/**
	 * The id of the process definition of the process instance.
	 */
	@JsonProperty("processDefinitionId")
	private String processDefinitionId;
	/**
	 * The business key of this process instance.
	 */
	@JsonProperty("businessKey")
	private String businessKey;
	/**
	 * The id of the root process instance associated with this process instance.
	 */
	@JsonProperty("rootProcessInstanceId")
	private String rootProcessInstanceId;
	/**
	 * The id of the case instance associated with this process instance.
	 */
	@JsonProperty("caseInstanceId")
	private String caseInstanceId;
	/**
	 * returns true if the process instance is suspended
	 */
	@JsonProperty("suspended")
	private boolean suspended;

	public ProcessInstanceDTO() {
		super();
	}

	public ProcessInstanceDTO(String processInstanceId, String processDefinitionId, String businessKey,
			boolean suspended) {
		super();
		this.processInstanceId = processInstanceId;
		this.processDefinitionId = processDefinitionId;
		this.businessKey = businessKey;
		this.suspended = suspended;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isEnded() {
		return ended;
	}

	public void setEnded(boolean ended) {
		this.ended = ended;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public String getBusinessKey() {
		return businessKey;
	}

	public void setBusinessKey(String businessKey) {
		this.businessKey = businessKey;
	}

	public String getRootProcessInstanceId() {
		return rootProcessInstanceId;
	}

	public void setRootProcessInstanceId(String rootProcessInstanceId) {
		this.rootProcessInstanceId = rootProcessInstanceId;
	}

	public String getCaseInstanceId() {
		return caseInstanceId;
	}

	public void setCaseInstanceId(String caseInstanceId) {
		this.caseInstanceId = caseInstanceId;
	}

	public boolean isSuspended() {
		return suspended;
	}

	public void setSuspended(boolean suspended) {
		this.suspended = suspended;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof ProcessInstanceDTO))
			return false;
		ProcessInstanceDTO that = (ProcessInstanceDTO) o;
		return Objects.equals(getProcessInstanceId(), that.getProcessInstanceId());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getProcessInstanceId());
	}
}

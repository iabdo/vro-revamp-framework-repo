package com.mazeed.vro.process.util;

import java.util.Collections;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;


public class RESTUtilities {
	public static <T> ResponseEntity<T> callREST(String url, HttpMethod method, MediaType mediaType,
			ParameterizedTypeReference<T> type, MultiValueMap<String, String> headerParams, T body) {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(mediaType));
		if (headerParams != null)
			headers.addAll(headerParams);

		HttpEntity<T> entity = null;
		if (method == HttpMethod.POST || method == HttpMethod.PUT) {
			entity = new HttpEntity<T>(body, headers);

		} else {
			entity = new HttpEntity<T>(headers);
		}

		ResponseEntity<T> restResponse = restTemplate.exchange(url, method, entity, type);
		return restResponse;
	}

	public static <T> ResponseEntity<PagedResources<T>> callRESTForList(String url, HttpMethod method, MediaType mediaType,
			ParameterizedTypeReference<PagedResources<T>> type, MultiValueMap<String, String> headerParams, T body) {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(mediaType));
		if (headerParams != null)
			headers.addAll(headerParams);

		HttpEntity<T> entity = null;
		if (method == HttpMethod.POST || method == HttpMethod.PUT) {
			entity = new HttpEntity<T>(body, headers);

		} else {
			entity = new HttpEntity<T>(headers);
		}

		ResponseEntity<PagedResources<T>> restResponse = restTemplate.exchange(url, method, entity, type);
		return restResponse;
	}

}

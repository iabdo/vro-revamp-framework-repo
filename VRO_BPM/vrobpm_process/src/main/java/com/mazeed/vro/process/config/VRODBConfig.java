package com.mazeed.vro.process.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "vroEntityManagerFactory", transactionManagerRef = "vroTransactionManager", basePackages = {
		"com.mazeed.vro.core", "com.mazeed.vro.core.repository" })
public class VRODBConfig {

	@Bean(name = "vroDataSource")
	@ConfigurationProperties(prefix = "vro.datasource")
	public DataSource vroDataSource() {
		return DataSourceBuilder.create().build();
	}

	@Bean(name = "vroEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean vroEntityManagerFactory(EntityManagerFactoryBuilder builder,
			@Qualifier("vroDataSource") DataSource dataSource) {
		LocalContainerEntityManagerFactoryBean em = builder.dataSource(dataSource)
				.packages("com.mazeed.vro.core.entity").persistenceUnit("vro").build();
		
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setGenerateDdl(true);
		em.setJpaVendorAdapter(vendorAdapter);
		return em;
	}

	@Bean(name = "vroTransactionManager")
	public PlatformTransactionManager vroTransactionManager(
			@Qualifier("vroEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}
}
package com.mazeed.vro.process;

import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import com.mazeed.vro.process.delegate.RetrieveMandateInfoDelegate;
import com.mazeed.vro.process.delegate.SaveMandateInfoDelegate;

@SpringBootApplication
@EnableProcessApplication
@ComponentScan(basePackages = { "com.mazeed.vro" })
public class ProcessApp {
	public static void main(String[] args) {
		new SpringApplicationBuilder(ProcessApp.class).properties("spring.config.name:vrobpm_process").build()
				.run(args);
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////// Delegate Bean
	////////////////////////////////////////////////////////////////////////////////////////// Definitions///////////////////////////////
	@Bean
	public RetrieveMandateInfoDelegate retrieveMandateInfoDelegate() {
		return new RetrieveMandateInfoDelegate();
	}

	@Bean
	public SaveMandateInfoDelegate saveMandateInfoDelegate() {
		return new SaveMandateInfoDelegate();
	}
	//////////////////////////////////////////////////////////////////////////////////////////
}
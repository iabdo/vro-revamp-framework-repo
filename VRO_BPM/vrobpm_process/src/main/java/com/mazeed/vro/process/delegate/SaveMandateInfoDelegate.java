package com.mazeed.vro.process.delegate;

import java.util.Arrays;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mazeed.vro.process.dto.MandateDTO;
import com.mazeed.vro.process.util.RESTUtilities;

@Service
public class SaveMandateInfoDelegate implements JavaDelegate {
	@Value("${env.endpoint.vro-api}")
	String apiBaseUrl;

	public void execute(DelegateExecution execution) throws Exception {

		ObjectMapper mapper = new ObjectMapper();

		String mandateJson = (String) execution.getVariable("updatedMandateJson");
		
		MandateDTO mandate = (mandateJson != null && !mandateJson.isEmpty()) ? 
							 mapper.readValue(mandateJson, MandateDTO.class) :
							 mapper.convertValue(execution.getVariable("mandateBO"), MandateDTO.class);

		// Update the mandate data
		String token = execution.getVariable("bearer_token") != null ? execution.getVariable("bearer_token").toString() : "";
		
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("Authorization", String.format("Bearer %s", token));
        
		ResponseEntity<MandateDTO> restResponse = RESTUtilities.callREST(apiBaseUrl + "mandates/" + mandate.getId(),
				HttpMethod.PUT, MediaType.APPLICATION_JSON, new ParameterizedTypeReference<MandateDTO>() {}, headers, mandate);
		mandate = restResponse.getBody();

		execution.setVariable("mandateJson", mapper.writeValueAsString(mandate));
		execution.setVariable("updatedMandateJson", "");
		execution.setVariable("mandateBO", mandate);
	}

}

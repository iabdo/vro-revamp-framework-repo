package com.mazeed.vro.process.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "processEngineEntityManagerFactory", basePackages = {
		"com.mazeed.vro.process" })
public class ProcessEngineDBConfig {

	@Primary
	@Bean(name = "processEngineDataSource")
	@ConfigurationProperties(prefix = "spring.datasource")
	public DataSource processEngineDataSource() {
		return DataSourceBuilder.create().build();
	}

	@Primary
	@Bean(name = "processEngineEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean processEngineEntityManagerFactory(EntityManagerFactoryBuilder builder,
			@Qualifier("processEngineDataSource") DataSource dataSource) {
		LocalContainerEntityManagerFactoryBean em = builder.dataSource(dataSource)
				.packages("com.mazeed.vro.process.entity").persistenceUnit("process-engine").build();

		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setGenerateDdl(true);
		em.setJpaVendorAdapter(vendorAdapter);
		return em;
	}

	@Primary
	@Bean(name = "processEngineTransactionManager")
	public PlatformTransactionManager processEngineTransactionManager(
			@Qualifier("processEngineEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}
}
package com.mazeed.vro.process.util;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.task.IdentityLink;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import com.mazeed.vro.process.enumeration.UserAction;

public class TaskUtilities {
	public static boolean isUserAuthorizedOnTask(String taskId, String userId, Collection<? extends GrantedAuthority> userAuthorities, TaskService taskService, UserAction userAction) {
		List<IdentityLink> identityLinks = taskService.getIdentityLinksForTask(taskId);
		Set<String> authorities = AuthorityUtils.authorityListToSet(userAuthorities);

		if (identityLinks != null) {
			// The user can claim, complete, delegate the task only if he's a member of the candidate groups
			// and no other user is assigned to/ owning the task

			// check for user groups
			boolean foundMatchingGroups = authorities.stream().anyMatch(authority -> {
				boolean foundExistingAuthority = identityLinks.stream().anyMatch(identity -> {
					return identity.getGroupId() != null && identity.getGroupId().equals(authority);
				});
				return foundExistingAuthority;});

			
			// check for assignee/ owner
			boolean foundAnotherAssignedUser = identityLinks.stream().anyMatch(identityLink -> {
				return identityLink.getUserId() != null && !identityLink.getUserId().equals(userId);
			});
			
			
			if (foundMatchingGroups && !foundAnotherAssignedUser)
				return true;
			else
				return false;

		}

		return false;
	}
}

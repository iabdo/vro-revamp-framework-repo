package com.mazeed.vro.process.enumeration;

public enum UserAction {
	CLAIM, UNCLAIM, DELEGATE, RESOLVE, COMPLETE;
}

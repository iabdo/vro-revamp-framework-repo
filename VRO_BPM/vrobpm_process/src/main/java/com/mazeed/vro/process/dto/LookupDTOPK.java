package com.mazeed.vro.process.dto;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LookupDTOPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3496564380279352716L;

	@JsonProperty("lookupType")
	private String lookupType;

	@JsonProperty("lookupKey")
	private String lookupKey;

	public LookupDTOPK() {
		super();
	}

	public LookupDTOPK(String lookupType, String lookupKey) {
		super();
		this.lookupType = lookupType;
		this.lookupKey = lookupKey;
	}

	public String getLookupType() {
		return lookupType;
	}

	public void setLookupType(String lookupType) {
		this.lookupType = lookupType;
	}

	public String getLookupKey() {
		return lookupKey;
	}

	public void setLookupKey(String lookupKey) {
		this.lookupKey = lookupKey;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof LookupDTOPK))
			return false;
		LookupDTOPK that = (LookupDTOPK) o;
		return Objects.equals(getLookupType(), that.getLookupType()) && Objects.equals(getLookupKey(), that.getLookupKey());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getLookupType(), getLookupKey());
	}
}

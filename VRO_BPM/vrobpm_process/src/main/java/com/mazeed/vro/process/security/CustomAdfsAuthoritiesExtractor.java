package com.mazeed.vro.process.security;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.boot.autoconfigure.security.oauth2.resource.AuthoritiesExtractor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

public class CustomAdfsAuthoritiesExtractor implements AuthoritiesExtractor {

	@Override
	public List<GrantedAuthority> extractAuthorities(Map<String, Object> map) {
		String tempKey = "";
		String commaSeparatedAuthorities = "USER,";
		if (map != null) {
			Iterator<String> itr = map.keySet().iterator();
			while (itr.hasNext()) {
				tempKey = itr.next();
				if (tempKey.contains("role_")) {
					commaSeparatedAuthorities += map.get(tempKey) + ",";
				}
			}
		}
		return AuthorityUtils.commaSeparatedStringToAuthorityList(commaSeparatedAuthorities);
	}

}

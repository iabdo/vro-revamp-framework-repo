package com.mazeed.vro.process.controller;

import java.util.HashMap;
import java.util.Map;

import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mazeed.vro.process.dto.ProcessInstanceDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.Example;
import io.swagger.annotations.ExampleProperty;

@RestController
@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
@Api(tags = { "processinstances" })
public class ProcessController {
	@Autowired
	RuntimeService runtimeService;

	@ApiOperation(value = "Start a new process instance", response = ProcessInstanceDTO.class, produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully created a process instance"), @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
		@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"), @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"), @ApiResponse(code = 500, message = "Internal Error") })
	@ApiImplicitParams({ @ApiImplicitParam(name = "processVariables", dataType = "ProcessVariable", required = false,
	examples = @Example(value = {
		@ExampleProperty(value = "{\"mandateId\": \"402881d26b750000016b754559760001\"}", 
			mediaType = "application/json") })) })
	@RequestMapping(path = "/processinstances", method = RequestMethod.POST, produces = "application/json")
	@Authorization(value = "oauth2")
	public ResponseEntity<?> createProcessInstance(
		@ApiParam(name = "PROCESS_KEY", value = "The process definition key", required = true, example = "investigate_feasibility") 
		@RequestHeader(name = "PROCESS_KEY", required = true) String processKey,
		@ApiParam(name = "Authorization", value = "The bearer token value", required = true) 
		@RequestHeader(name = "Authorization", required = true) String bearerToken,
		@RequestBody Map<String, Object> processVariables) {

		if(processVariables == null) 
			processVariables = new HashMap<String, Object>();
		processVariables.put("bearer_token", bearerToken);
		
		ProcessInstance instance = runtimeService.startProcessInstanceByKey(processKey, processVariables);

		ProcessInstanceDTO instanceDTO = new ProcessInstanceDTO(instance.getProcessInstanceId(), instance.getProcessDefinitionId(), instance.getBusinessKey(),
			instance.isSuspended());

		return new ResponseEntity<>(instanceDTO, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Delete an existing process instance", produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully deleted a process instance"), @ApiResponse(code = 401, message = "You are not authorized to delete the resource"),
		@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"), @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"), @ApiResponse(code = 500, message = "Internal Error") })
	@RequestMapping(path = "/processinstances/{id}", method = RequestMethod.DELETE, produces = "application/json")
	@Authorization(value = "oauth2")
	public ResponseEntity<?> deleteProcessInstance(@PathVariable String id, 
		@RequestHeader(name = "DELETE_REASON", required = true) String deleteReason,
		@ApiParam(name = "Authorization", value = "The bearer token value", required = true) 
		@RequestHeader(name = "Authorization", required = true) String bearerToken) {

		runtimeService.deleteProcessInstance(id, deleteReason);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
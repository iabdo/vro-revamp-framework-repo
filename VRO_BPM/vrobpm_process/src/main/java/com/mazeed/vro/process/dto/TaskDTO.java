package com.mazeed.vro.process.dto;

import java.io.Serializable;
import java.util.Date;

import org.camunda.bpm.engine.task.DelegationState;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TaskDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8484680904615729530L;

	@JsonProperty("id")
	private String id;

	@JsonProperty("name")
	private String name;

	@JsonProperty("description")
	private String description;

	/**
	 * indication of how important/urgent this task is with a number between 0 and
	 * 100 where higher values mean a higher priority and lower values mean lower
	 * priority: [0..19] lowest, [20..39] low, [40..59] normal, [60..79] high
	 * [80..100] highest
	 */
	@JsonProperty("priority")
	private int priority;

	@JsonProperty("owner")
	private String owner;

	@JsonProperty("assignee")
	private String assignee;

	@JsonProperty("delegationState")
	private DelegationState delegationState;

	@JsonProperty("processInstanceId")
	private String processInstanceId;

	@JsonProperty("executionId")
	private String executionId;

	@JsonProperty("processDefinitionId")
	private String processDefinitionId;

	@JsonProperty("caseInstanceId")
	private String caseInstanceId;

	@JsonProperty("createTime")
	private Date createTime;

	@JsonProperty("taskDefinitionKey")
	private String taskDefinitionKey;

	@JsonProperty("dueDate")
	private Date dueDate;

	@JsonProperty("followUpDate")
	private Date followUpDate;

	@JsonProperty("parentTaskId")
	private String parentTaskId;

	@JsonProperty("suspended")
	private boolean suspended;

	@JsonProperty("formKey")
	private String formKey;

	@JsonProperty("tenantId")
	private String tenantId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public DelegationState getDelegationState() {
		return delegationState;
	}

	public void setDelegationState(DelegationState delegationState) {
		this.delegationState = delegationState;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getExecutionId() {
		return executionId;
	}

	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public String getCaseInstanceId() {
		return caseInstanceId;
	}

	public void setCaseInstanceId(String caseInstanceId) {
		this.caseInstanceId = caseInstanceId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getTaskDefinitionKey() {
		return taskDefinitionKey;
	}

	public void setTaskDefinitionKey(String taskDefinitionKey) {
		this.taskDefinitionKey = taskDefinitionKey;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Date getFollowUpDate() {
		return followUpDate;
	}

	public void setFollowUpDate(Date followUpDate) {
		this.followUpDate = followUpDate;
	}

	public String getParentTaskId() {
		return parentTaskId;
	}

	public void setParentTaskId(String parentTaskId) {
		this.parentTaskId = parentTaskId;
	}

	public boolean isSuspended() {
		return suspended;
	}

	public void setSuspended(boolean suspended) {
		this.suspended = suspended;
	}

	public String getFormKey() {
		return formKey;
	}

	public void setFormKey(String formKey) {
		this.formKey = formKey;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
}

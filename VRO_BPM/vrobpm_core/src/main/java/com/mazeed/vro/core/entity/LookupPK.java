package com.mazeed.vro.core.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class LookupPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2043743707200820833L;

	@Column(name = "lookup_type")
	private String lookupType;

	@Column(name = "lookup_key")
	private String lookupKey;

	public LookupPK() {
		super();
	}

	public LookupPK(String lookupType, String lookupKey) {
		super();
		this.lookupType = lookupType;
		this.lookupKey = lookupKey;
	}

	public String getLookupType() {
		return lookupType;
	}

	public void setLookupType(String lookupType) {
		this.lookupType = lookupType;
	}

	public String getLookupKey() {
		return lookupKey;
	}

	public void setLookupKey(String lookupKey) {
		this.lookupKey = lookupKey;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof LookupPK))
			return false;
		LookupPK that = (LookupPK) o;
		return Objects.equals(getLookupType(), that.getLookupType())
				&& Objects.equals(getLookupKey(), that.getLookupKey());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getLookupType(), getLookupKey());
	}
}

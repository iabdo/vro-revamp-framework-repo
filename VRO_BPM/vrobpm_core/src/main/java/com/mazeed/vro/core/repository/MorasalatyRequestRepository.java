package com.mazeed.vro.core.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mazeed.vro.core.entity.MorasalatyRequest;

@Repository
@Transactional
public interface MorasalatyRequestRepository extends JpaRepository<MorasalatyRequest, String> {

	List<MorasalatyRequest> findByMandateIsNull();
}
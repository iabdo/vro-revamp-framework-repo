package com.mazeed.vro.core.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "LOOKUP")
//, uniqueConstraints = @UniqueConstraint(columnNames = { "lookup_type", "lookup_key" }))
public class Lookup implements Serializable {

	@EmbeddedId
    private LookupPK lookupPK;
    
	@Column(name = "lookup_value")
	private String lookupValue;
	
	@Column(name = "lookup_value_ar")
	private String lookupValueAr;

	public Lookup() {
		super();
	}
	
	public Lookup(String lookupType, String lookupKey) {
		super();
		this.lookupPK = new LookupPK(lookupType, lookupKey);
	}
	
	public Lookup(String lookupType, String lookupKey, String lookupValue, String lookupValueAr) {
		super();
		this.lookupPK = new LookupPK(lookupType, lookupKey);
		this.lookupValue = lookupValue;
		this.lookupValueAr = lookupValueAr;
	}

	public LookupPK getLookupPK() {
		return lookupPK;
	}

	public void setLookupPK(LookupPK lookupPK) {
		this.lookupPK = lookupPK;
	}

	public String getLookupValue() {
		return lookupValue;
	}

	public void setLookupValue(String lookupValue) {
		this.lookupValue = lookupValue;
	}

	public String getLookupValueAr() {
		return lookupValueAr;
	}

	public void setLookupValueAr(String lookupValueAr) {
		this.lookupValueAr = lookupValueAr;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Lookup))
			return false;
		Lookup that = (Lookup) o;
		return Objects.equals(getLookupPK(), that.getLookupPK());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getLookupPK());
	}
}

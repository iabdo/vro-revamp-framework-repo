package com.mazeed.vro.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mazeed.vro.core.entity.Mandate;

@Repository
@Transactional
public interface MandateRepository extends JpaRepository<Mandate, String> {

}
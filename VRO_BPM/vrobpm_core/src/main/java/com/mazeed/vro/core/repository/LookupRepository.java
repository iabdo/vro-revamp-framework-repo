package com.mazeed.vro.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mazeed.vro.core.entity.Lookup;
import com.mazeed.vro.core.entity.LookupPK;

@Repository
@Transactional
public interface LookupRepository extends JpaRepository<Lookup, LookupPK> {

}
package com.mazeed.vro.core.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "MORASALATY_REQUEST")
public class MorasalatyRequest extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9057368028074301465L;

	@Column(name = "incoming_number")
	private String incomingNumber;

	@Column(name = "original_incoming_number")
	private String originalIncomingNumber;

	@Column(name = "outgoing_number")
	private String outgoingNumber;

	@Column(name = "subject")
	private String subject;

	@Column(name = "request_date")
	private Date requestDate;

	@Column(name = "start_date")
	private Date startDate;

	@Column(name = "end_date")
	private Date endDate;

	@Column(name = "duration")
	private int duration;

	@ManyToOne(cascade = { CascadeType.MERGE }, fetch = FetchType.EAGER)
	@JoinColumns({ @JoinColumn(name = "source_luType", referencedColumnName = "lookup_type"),
			@JoinColumn(name = "source_luKey", referencedColumnName = "lookup_key") })
	private Lookup source;

	@ManyToOne(cascade = { CascadeType.MERGE }, fetch = FetchType.EAGER)
	@JoinColumns({ @JoinColumn(name = "confidentiality_level_luType", referencedColumnName = "lookup_type"),
			@JoinColumn(name = "confidentiality_level_luKey", referencedColumnName = "lookup_key") })
	private Lookup confidentialityLevel;

	@Column(name = "linked_with_existing_mandate")
	private boolean linkedWithExistingMandate;

	@Column(name = "request_scan")
	private String requestScan;

	@JsonBackReference
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "mandate_uuid")
	private Mandate mandate;

	public String getIncomingNumber() {
		return incomingNumber;
	}

	public void setIncomingNumber(String incomingNumber) {
		this.incomingNumber = incomingNumber;
	}

	public String getOriginalIncomingNumber() {
		return originalIncomingNumber;
	}

	public void setOriginalIncomingNumber(String originalIncomingNumber) {
		this.originalIncomingNumber = originalIncomingNumber;
	}

	public String getOutgoingNumber() {
		return outgoingNumber;
	}

	public void setOutgoingNumber(String outgoingNumber) {
		this.outgoingNumber = outgoingNumber;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public Lookup getSource() {
		return source;
	}

	public void setSource(Lookup source) {
		this.source = source;
	}

	public Lookup getConfidentialityLevel() {
		return confidentialityLevel;
	}

	public void setConfidentialityLevel(Lookup confidentialityLevel) {
		this.confidentialityLevel = confidentialityLevel;
	}

	public boolean isLinkedWithExistingMandate() {
		return linkedWithExistingMandate;
	}

	public void setLinkedWithExistingMandate(boolean linkedWithExistingMandate) {
		this.linkedWithExistingMandate = linkedWithExistingMandate;
	}

	public String getRequestScan() {
		return requestScan;
	}

	public void setRequestScan(String requestScan) {
		this.requestScan = requestScan;
	}

	public Mandate getMandate() {
		return mandate;
	}

	public void setMandate(Mandate mandate) {
		this.mandate = mandate;
	}
}
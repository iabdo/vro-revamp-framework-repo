package com.mazeed.vro.core.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "MANDATE")
public class Mandate extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3518764389006826885L;

	@Column(name = "incoming_number")
	private String incomingNumber;

	@Column(name = "original_incoming_number")
	private String originalIncomingNumber;

	@Column(name = "outgoing_number")
	private String outgoingNumber;
	/********************************************************************************************/
	/*
	 * The attributes in the following section are duplicated from
	 * MorasalatyRequest, because sometime those attributes come empty from
	 * Morasalaty system, and they need to be filled in VRO system, so they are
	 * duplicated in Mandate again, to keep track of the data
	 */
	/********************************************************************************************/
	@Column(name = "subject")
	private String subject;

	@Column(name = "start_date")
	private Date startDate;

	@Column(name = "end_date")
	private Date endDate;

	@Column(name = "duration")
	private int duration;

	@ManyToOne(cascade = { CascadeType.MERGE }, fetch = FetchType.EAGER)
	@JoinColumns({ @JoinColumn(name = "source_luType", referencedColumnName = "lookup_type"),
			@JoinColumn(name = "source_luKey", referencedColumnName = "lookup_key") })
	private Lookup source;

	@ManyToOne(cascade = { CascadeType.MERGE }, fetch = FetchType.EAGER)
	@JoinColumns({ @JoinColumn(name = "confidentiality_level_luType", referencedColumnName = "lookup_type"),
			@JoinColumn(name = "confidentiality_level_luKey", referencedColumnName = "lookup_key") })
	private Lookup confidentialityLevel;
	/********************************************************************************************/
	/********************************************************************************************/

	@Column(name = "mandate_consultant_username")
	private String mandateConsultantUsername;

	@Column(name = "directed_by")
	private String directedBy;

	@ManyToOne(cascade = { CascadeType.MERGE }, fetch = FetchType.EAGER)
	@JoinColumns({ @JoinColumn(name = "completion_status_luType", referencedColumnName = "lookup_type"),
			@JoinColumn(name = "completion_status_luKey", referencedColumnName = "lookup_key") })
	private Lookup completionStatus;

	@ManyToOne(cascade = { CascadeType.MERGE }, fetch = FetchType.EAGER)
	@JoinColumns({ @JoinColumn(name = "execution_status_luType", referencedColumnName = "lookup_type"),
			@JoinColumn(name = "execution_status_luKey", referencedColumnName = "lookup_key") })
	private Lookup executionStatus;

	@Column(name = "full_subject")
	private String fullSubject;

	@Column(name = "general_notes")
	private String generalNotes;

	@ManyToOne(cascade = { CascadeType.MERGE }, fetch = FetchType.EAGER)
	@JoinColumns({ @JoinColumn(name = "wallet_luType", referencedColumnName = "lookup_type"),
			@JoinColumn(name = "wallet_luKey", referencedColumnName = "lookup_key") })
	private Lookup wallet;

	@ManyToOne(cascade = { CascadeType.MERGE }, fetch = FetchType.EAGER)
	@JoinColumns({ @JoinColumn(name = "minister_priority_luType", referencedColumnName = "lookup_type"),
			@JoinColumn(name = "minister_priority_luKey", referencedColumnName = "lookup_key") })
	private Lookup ministerPriority;

	@Column(name = "minister_priority_notes")
	private String ministerPriorityNotes;

	@ManyToOne(cascade = { CascadeType.MERGE }, fetch = FetchType.EAGER)
	@JoinColumns({ @JoinColumn(name = "minister_direction_luType", referencedColumnName = "lookup_type"),
			@JoinColumn(name = "minister_direction_luKey", referencedColumnName = "lookup_key") })
	private Lookup ministerDirection;

	@Column(name = "minister_direction_notes")
	private String ministerDirectionNotes;

	@Column(name = "business_owner_username")
	private String businessOwnerUsername;
	
	@Column(name = "business_owner_email")
	private String businessOwnerEmail;

	@Column(name = "needs_extension")
	private boolean needsExtension;

	
	@JsonManagedReference
	@OneToMany(mappedBy = "mandate", cascade = { CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REFRESH, CascadeType.REMOVE }, fetch = FetchType.EAGER)
	private List<MorasalatyRequest> morasalatyRequests;

	public String getIncomingNumber() {
		return incomingNumber;
	}

	public void setIncomingNumber(String incomingNumber) {
		this.incomingNumber = incomingNumber;
	}

	public String getOriginalIncomingNumber() {
		return originalIncomingNumber;
	}

	public void setOriginalIncomingNumber(String originalIncomingNumber) {
		this.originalIncomingNumber = originalIncomingNumber;
	}

	public String getOutgoingNumber() {
		return outgoingNumber;
	}

	public void setOutgoingNumber(String outgoingNumber) {
		this.outgoingNumber = outgoingNumber;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public Lookup getSource() {
		return source;
	}

	public void setSource(Lookup source) {
		this.source = source;
	}

	public Lookup getConfidentialityLevel() {
		return confidentialityLevel;
	}

	public void setConfidentialityLevel(Lookup confidentialityLevel) {
		this.confidentialityLevel = confidentialityLevel;
	}

	public String getMandateConsultantUsername() {
		return mandateConsultantUsername;
	}

	public void setMandateConsultantUsername(String mandateConsultantUsername) {
		this.mandateConsultantUsername = mandateConsultantUsername;
	}

	public String getDirectedBy() {
		return directedBy;
	}

	public void setDirectedBy(String directedBy) {
		this.directedBy = directedBy;
	}

	public Lookup getCompletionStatus() {
		return completionStatus;
	}

	public void setCompletionStatus(Lookup completionStatus) {
		this.completionStatus = completionStatus;
	}

	public Lookup getExecutionStatus() {
		return executionStatus;
	}

	public void setExecutionStatus(Lookup executionStatus) {
		this.executionStatus = executionStatus;
	}

	public String getFullSubject() {
		return fullSubject;
	}

	public void setFullSubject(String fullSubject) {
		this.fullSubject = fullSubject;
	}

	public String getGeneralNotes() {
		return generalNotes;
	}

	public void setGeneralNotes(String generalNotes) {
		this.generalNotes = generalNotes;
	}

	public Lookup getWallet() {
		return wallet;
	}

	public void setWallet(Lookup wallet) {
		this.wallet = wallet;
	}

	public Lookup getMinisterPriority() {
		return ministerPriority;
	}

	public void setMinisterPriority(Lookup ministerPriority) {
		this.ministerPriority = ministerPriority;
	}

	public String getMinisterPriorityNotes() {
		return ministerPriorityNotes;
	}

	public void setMinisterPriorityNotes(String ministerPriorityNotes) {
		this.ministerPriorityNotes = ministerPriorityNotes;
	}

	public Lookup getMinisterDirection() {
		return ministerDirection;
	}

	public void setMinisterDirection(Lookup ministerDirection) {
		this.ministerDirection = ministerDirection;
	}

	public String getMinisterDirectionNotes() {
		return ministerDirectionNotes;
	}

	public void setMinisterDirectionNotes(String ministerDirectionNotes) {
		this.ministerDirectionNotes = ministerDirectionNotes;
	}

	public String getBusinessOwnerUsername() {
		return businessOwnerUsername;
	}

	public void setBusinessOwnerUsername(String businessOwnerUsername) {
		this.businessOwnerUsername = businessOwnerUsername;
	}

	public String getBusinessOwnerEmail() {
		return businessOwnerEmail;
	}

	public void setBusinessOwnerEmail(String businessOwnerEmail) {
		this.businessOwnerEmail = businessOwnerEmail;
	}

	public boolean isNeedsExtension() {
		return needsExtension;
	}

	public void setNeedsExtension(boolean needsExtension) {
		this.needsExtension = needsExtension;
	}

	public List<MorasalatyRequest> getMorasalatyRequests() {
		return morasalatyRequests;
	}

	public void setMorasalatyRequests(List<MorasalatyRequest> morasalatyRequests) {
		this.morasalatyRequests = morasalatyRequests;
	}
}

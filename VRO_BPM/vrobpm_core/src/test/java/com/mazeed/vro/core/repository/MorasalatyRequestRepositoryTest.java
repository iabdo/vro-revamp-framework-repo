package com.mazeed.vro.core.repository;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.mazeed.vro.core.CoreApp;
import com.mazeed.vro.core.entity.MorasalatyRequest;

@RunWith(SpringRunner.class)
@DataJpaTest
@SpringBootTest(classes={CoreApp.class})
public class MorasalatyRequestRepositoryTest {
	@Autowired
	private MorasalatyRequestRepository morasalatyRepo;

	@Test
	public void whenFindAll_thenReturnAllSavedRequests() {
		MorasalatyRequest request = new MorasalatyRequest();
		request.setIncomingNumber("1123213");
		request.setOutgoingNumber("4565654");

		morasalatyRepo.save(request);

		request = new MorasalatyRequest();
		request.setIncomingNumber("9898980");
		request.setOutgoingNumber("7566443");

		morasalatyRepo.save(request);

		List<MorasalatyRequest> requests = morasalatyRepo.findAll();

		assertEquals(2, requests.size());

		assertEquals("1123213", requests.get(0).getIncomingNumber());
		assertEquals("4565654", requests.get(0).getOutgoingNumber());

		assertEquals("9898980", requests.get(1).getIncomingNumber());
		assertEquals("7566443", requests.get(1).getOutgoingNumber());
	}

	@Test
	public void whenDelete_thenRemoveTheRequest() {
		MorasalatyRequest request = new MorasalatyRequest();
		request.setIncomingNumber("1123213");
		request.setOutgoingNumber("4565654");

		morasalatyRepo.save(request);

		List<MorasalatyRequest> foundRequests = morasalatyRepo.findAll();
		morasalatyRepo.delete(foundRequests.get(0));

		foundRequests = morasalatyRepo.findAll();
		assertEquals(0, foundRequests.size());
	}

	@Test
	public void whenUpdateTheRequest_thenInfoIsUpdated() {
		MorasalatyRequest request = new MorasalatyRequest();
		request.setIncomingNumber("1123213");

		morasalatyRepo.save(request);

		request.setOutgoingNumber("4565654");
		morasalatyRepo.save(request);

		List<MorasalatyRequest> requests = morasalatyRepo.findAll();
		assertEquals(1, requests.size());
		assertEquals("4565654", requests.get(0).getOutgoingNumber());
	}

	@Test
	public void whenNotFound_returnNull() {
		Optional<MorasalatyRequest> request = morasalatyRepo.findById("1");
		assertEquals(false, request.isPresent());
	}
}
package com.mazeed.vro.core.repository;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.mazeed.vro.core.CoreApp;
import com.mazeed.vro.core.entity.Lookup;
import com.mazeed.vro.core.entity.Mandate;

@RunWith(SpringRunner.class)
@DataJpaTest
@SpringBootTest(classes={CoreApp.class})
public class MandateRepositoryTest {
	@Autowired
	private MandateRepository mandateRepo;
	
	@Autowired
	private LookupRepository lookupRepo;

	@Test
	public void whenFindAll_thenReturnAllSavedRequests() {
		
		Lookup lookup = new Lookup("confidentiality_level", "NRM", "Normal", "عادي");
		lookupRepo.save(lookup);
		lookupRepo.flush();
		
		Mandate mandate = new Mandate();
		mandate.setConfidentialityLevel(new Lookup("confidentiality_level", "NRM", "Normal", "عادي"));
		mandate.setBusinessOwnerUsername("iabdo");

		mandateRepo.save(mandate);

		mandate = new Mandate();
		mandate.setConfidentialityLevel(new Lookup("confidentiality_level", "NRM"));
		mandate.setBusinessOwnerUsername("amohamed");

		mandateRepo.save(mandate);

		List<Mandate> mandates = mandateRepo.findAll();

		assertEquals(2, mandates.size());

		assertEquals("NRM", mandates.get(0).getConfidentialityLevel().getLookupPK().getLookupKey());
		assertEquals("iabdo", mandates.get(0).getBusinessOwnerUsername());

		assertEquals("NRM", mandates.get(1).getConfidentialityLevel().getLookupPK().getLookupKey());
		assertEquals("amohamed", mandates.get(1).getBusinessOwnerUsername());
	}

	@Test
	public void whenDelete_thenRemoveTheRequest() {
		Lookup lookup = new Lookup("confidentiality_level", "NRM", "Normal", "عادي");
		lookupRepo.save(lookup);
		lookupRepo.flush();
		
		Mandate mandate = new Mandate();
		mandate.setConfidentialityLevel(new Lookup("confidentiality_level", "NRM"));
		mandate.setBusinessOwnerUsername("amohamed");

		mandateRepo.save(mandate);

		List<Mandate> foundMandates = mandateRepo.findAll();
		mandateRepo.delete(foundMandates.get(0));

		foundMandates = mandateRepo.findAll();
		assertEquals(0, foundMandates.size());
	}

	@Test
	public void whenUpdateTheRequest_thenInfoIsUpdated() {
		Lookup lookup = new Lookup("confidentiality_level", "NRM", "Normal", "عادي");
		lookupRepo.save(lookup);
		lookupRepo.flush();
		
		Mandate mandate = new Mandate();
		mandate.setConfidentialityLevel(new Lookup("confidentiality_level", "NRM"));
		
		mandate.setBusinessOwnerUsername("amohamed");
		mandateRepo.save(mandate);

		mandate.setBusinessOwnerUsername("iabdo");
		mandateRepo.save(mandate);

		List<Mandate> mandates = mandateRepo.findAll();
		assertEquals(1, mandates.size());
		assertEquals("iabdo", mandates.get(0).getBusinessOwnerUsername());
	}

	@Test
	public void whenNotFound_returnNull() {
		Optional<Mandate> mandate = mandateRepo.findById("1");
		assertEquals(false, mandate.isPresent());
	}
}
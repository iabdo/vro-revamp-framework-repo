package com.mazeed.vro.api;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@ComponentScan(basePackages = { "com.mazeed.vro" })
@Slf4j
public class APIApp {
	public static void main(String[] args) {
		new SpringApplicationBuilder(APIApp.class).properties("spring.config.name:vrobpm_api").build().run(args);
	}
}

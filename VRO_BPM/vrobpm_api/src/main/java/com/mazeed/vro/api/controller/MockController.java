package com.mazeed.vro.api.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mazeed.vro.core.entity.Lookup;
import com.mazeed.vro.core.entity.MorasalatyRequest;
import com.mazeed.vro.core.repository.LookupRepository;
import com.mazeed.vro.core.repository.MorasalatyRequestRepository;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class MockController {

	@Autowired
	LookupRepository lookupRepo;

	@Autowired
	MorasalatyRequestRepository morasalatyRepo;

	@RequestMapping(path = "/mock", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String morasalatyRequests() {
		Lookup lookup = new Lookup("SOURCE", "SRC1", "Source 1", "مصدر 1");
		lookupRepo.save(lookup);
		lookup = new Lookup("CONFIDENTIALITY_LVL", "NRM", "Normal", "عادي");
		lookupRepo.save(lookup);
		lookup = new Lookup("COMPLETION_STATUS", "INPROG", "In Progress", "تحت التقدم");
		lookupRepo.save(lookup);
		lookup = new Lookup("EXECUTION_STATUS", "DELAY", "Delayed", "متأخر");
		lookupRepo.save(lookup);
		lookup = new Lookup("WALLET", "WALLET1", "Wallet 1", "المحفظة 1");
		lookupRepo.save(lookup);
		lookup = new Lookup("MINISTER_PRIORITY", "A+", "A+", "أ+");
		lookupRepo.save(lookup);
		lookup = new Lookup("MINISTER_DIRECTION", "EXTND", "Extend", "تمديد");
		lookupRepo.save(lookup);
		lookupRepo.flush();

		MorasalatyRequest request = new MorasalatyRequest();
		request.setIncomingNumber("434243243244");
		request.setOutgoingNumber("8787879999");
		request.setSubject("Increase Employment Rate in KSA");
		request.setRequestDate(new Date());
		request.setStartDate(new Date(2019, 8, 20));
		request.setEndDate(new Date(2019, 10, 25));
		request.setDuration(90);
		request.setSource(new Lookup("SOURCE", "SRC1"));
		request.setConfidentialityLevel(new Lookup("CONFIDENTIALITY_LVL", "NRM"));
		request.setLinkedWithExistingMandate(false);
		morasalatyRepo.save(request);

		request = new MorasalatyRequest();
		request.setIncomingNumber("5656565563");
		request.setOutgoingNumber("3232323332");
		request.setSubject("Increase Women Employment Rate in KSA");
		request.setRequestDate(new Date());
		request.setStartDate(new Date(2019, 8, 20));
		request.setEndDate(new Date(2019, 10, 25));
		request.setDuration(90);
		request.setSource(new Lookup("SOURCE", "SRC1"));
		request.setConfidentialityLevel(new Lookup("CONFIDENTIALITY_LVL", "NRM"));
		request.setLinkedWithExistingMandate(false);
		morasalatyRepo.save(request);

		request = new MorasalatyRequest();
		request.setIncomingNumber("77676767677");
		request.setOutgoingNumber("989007676776");
		request.setSubject("Increase Schools in KSA");
		request.setRequestDate(new Date());
		request.setStartDate(new Date(2019, 8, 20));
		request.setEndDate(new Date(2019, 10, 25));
		request.setDuration(90);
		request.setSource(new Lookup("SOURCE", "SRC1"));
		request.setConfidentialityLevel(new Lookup("CONFIDENTIALITY_LVL", "NRM"));
		request.setLinkedWithExistingMandate(false);
		morasalatyRepo.save(request);
		morasalatyRepo.flush();

		return "SUCCESS";
	}
}
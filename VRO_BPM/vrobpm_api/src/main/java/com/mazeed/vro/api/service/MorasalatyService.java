package com.mazeed.vro.api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mazeed.vro.core.entity.Mandate;
import com.mazeed.vro.core.entity.MorasalatyRequest;
import com.mazeed.vro.core.repository.MorasalatyRequestRepository;

@Service
public class MorasalatyService {

	@Autowired
	MorasalatyRequestRepository morasalatyReqRepo;

	public List<MorasalatyRequest> findAll(boolean isStanding) {
		if (isStanding) {
			return morasalatyReqRepo.findByMandateIsNull();
		}

		return morasalatyReqRepo.findAll();
	}

	public MorasalatyRequest findById(String requestId) {
		Optional<MorasalatyRequest> existingMorasalatyRequest = morasalatyReqRepo.findById(requestId);
		if (existingMorasalatyRequest == null || !existingMorasalatyRequest.isPresent()) {
			throw new EntityNotFoundException("Cannot get request with id='" + requestId + "', as it doesn't exist");
		}
		return existingMorasalatyRequest.get();
	}

	public MorasalatyRequest create(MorasalatyRequest request) {
		morasalatyReqRepo.save(request);
		morasalatyReqRepo.flush();

		return request;
	}

	public MorasalatyRequest update(String requestId, MorasalatyRequest request) {
		Optional<MorasalatyRequest> existingMorasalatyRequest = morasalatyReqRepo.findById(requestId);
		if (existingMorasalatyRequest == null || !existingMorasalatyRequest.isPresent()) {
			throw new EntityNotFoundException("Cannot update request with id='" + requestId + "', as it doesn't exist");
		}
		request.setId(requestId);
		morasalatyReqRepo.save(request);
		morasalatyReqRepo.flush();

		return request;
	}

	public MorasalatyRequest update(String requestId, MorasalatyRequest request, Mandate mandate) {
		Optional<MorasalatyRequest> existingMorasalatyRequest = morasalatyReqRepo.findById(requestId);
		if (existingMorasalatyRequest == null || !existingMorasalatyRequest.isPresent()) {
			throw new EntityNotFoundException("Cannot update request with id='" + requestId + "', as it doesn't exist");
		}
		request.setId(requestId);
		request.setMandate(mandate);
		morasalatyReqRepo.save(request);
		morasalatyReqRepo.flush();

		return request;
	}

	public void delete(String requestId) {
		Optional<MorasalatyRequest> existingMorasalatyRequest = morasalatyReqRepo.findById(requestId);
		if (existingMorasalatyRequest == null || !existingMorasalatyRequest.isPresent()) {
			throw new EntityNotFoundException("Cannot delete request with id='" + requestId + "', as it doesn't exist");
		}
		morasalatyReqRepo.delete(existingMorasalatyRequest.get());
		morasalatyReqRepo.flush();
	}
}
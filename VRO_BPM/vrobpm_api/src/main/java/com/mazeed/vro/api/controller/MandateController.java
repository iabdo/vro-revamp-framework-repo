package com.mazeed.vro.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mazeed.vro.api.service.MandateService;
import com.mazeed.vro.core.entity.Mandate;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(tags = { "mandates" })
public class MandateController {

	@Autowired
	MandateService mandateService;

	@ApiOperation(value = "View a list of available mandates", response = Iterable.class, produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(path = "/mandates", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> listMandates(@ApiParam(name = "Authorization", value = "The bearer token value", required = true) 
	@RequestHeader(name = "Authorization", required = true) String bearerToken) {
		List<Mandate> mandates = mandateService.findAll();
		return new ResponseEntity<>(mandates, HttpStatus.OK);
	}

	@ApiOperation(value = "View the details of a specific mandate by id", response = Mandate.class, produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved mandate"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(path = "/mandates/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getMandateById(@PathVariable String id,
		@ApiParam(name = "Authorization", value = "The bearer token value", required = true) 
	@RequestHeader(name = "Authorization", required = true) String bearerToken) {
		Mandate mandate = mandateService.findById(id);
		return new ResponseEntity<>(mandate, HttpStatus.OK);
	}

	@ApiOperation(value = "Create a mandate with/ without linking to morasalaty request", response = Mandate.class, produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully created mandate"),
			@ApiResponse(code = 401, message = "You are not authorized to create the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 500, message = "Internal Error")})
	@RequestMapping(path = "/mandates", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> createMandate(
			@RequestHeader(name = "MORASALATY_RQST_ID", required = false) String morasalatyReqId,
			@RequestBody Mandate mandate,
			@ApiParam(name = "Authorization", value = "The bearer token value", required = true) 
			@RequestHeader(name = "Authorization", required = true) String bearerToken) {
		if (morasalatyReqId != null && morasalatyReqId.trim() != "") {
			mandateService.create(morasalatyReqId, mandate);
		} // In case of an internal mandate
		else {
			mandateService.create(mandate);
		}
		return new ResponseEntity<>(mandate, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Update a specific mandate by id", response = Mandate.class, produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully updated mandate"),
			@ApiResponse(code = 401, message = "You are not authorized to update the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 500, message = "Internal Error")})
	@RequestMapping(path = "/mandates/{id}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<?> updateMandate(@PathVariable String id, @RequestBody Mandate mandate,
		@ApiParam(name = "Authorization", value = "The bearer token value", required = true) 
	@RequestHeader(name = "Authorization", required = true) String bearerToken) {
		mandateService.update(id, mandate);

		return new ResponseEntity<>(mandate, HttpStatus.OK);
	}

	@ApiOperation(value = "Delete a specific mandate by id", produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully deleted mandate"),
			@ApiResponse(code = 401, message = "You are not authorized to delete the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 500, message = "Internal Error")})
	@RequestMapping(path = "/mandates/{id}", method = RequestMethod.DELETE, produces = "application/json")
	public ResponseEntity<?> deleteMandate(@PathVariable String id,
		@ApiParam(name = "Authorization", value = "The bearer token value", required = true) 
	@RequestHeader(name = "Authorization", required = true) String bearerToken) {
		mandateService.delete(id);

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

}
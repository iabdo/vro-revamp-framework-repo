package com.mazeed.vro.api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mazeed.vro.core.entity.Mandate;
import com.mazeed.vro.core.entity.MorasalatyRequest;
import com.mazeed.vro.core.repository.MandateRepository;
import com.mazeed.vro.core.repository.MorasalatyRequestRepository;

@Service
public class MandateService {

	@Autowired
	MandateRepository mandateRepo;

	@Autowired
	MorasalatyService morasalatyService;

	@Autowired
	MorasalatyRequestRepository morasalatyReqRepo;

	public List<Mandate> findAll() {
		return mandateRepo.findAll();
	}

	public Mandate findById(String mandateId) {
		Optional<Mandate> existingMandate = mandateRepo.findById(mandateId);
		if (existingMandate == null || !existingMandate.isPresent()) {
			throw new EntityNotFoundException("Cannot get mandate with id='" + mandateId + "', as it doesn't exist");
		}
		return existingMandate.get();
	}

	public Mandate create(Mandate mandate) {
		mandateRepo.save(mandate);
		mandateRepo.flush();

		return mandate;
	}

	public Mandate create(String morasalatyRequestId, Mandate mandate) {
		MorasalatyRequest morasalatyRequest = morasalatyService.findById(morasalatyRequestId);

		mandateRepo.save(mandate);
		mandateRepo.flush();

		morasalatyRequest.setMandate(mandate);
		morasalatyReqRepo.save(morasalatyRequest);

		if (mandate.getMorasalatyRequests() == null)
			mandate.setMorasalatyRequests(new ArrayList<MorasalatyRequest>());

		mandate.getMorasalatyRequests().add(morasalatyRequest);
		return mandate;
	}

	public Mandate update(String mandateId, Mandate mandate) {
		Optional<Mandate> existingMandate = mandateRepo.findById(mandateId);
		if (existingMandate == null || !existingMandate.isPresent()) {
			throw new EntityNotFoundException("Cannot update mandate with id='" + mandateId + "', as it doesn't exist");
		}
		mandate.setId(mandateId);
		mandateRepo.save(mandate);
		mandateRepo.flush();

		return mandate;
	}

	public void delete(String mandateId) {
		Optional<Mandate> existingMandate = mandateRepo.findById(mandateId);
		if (existingMandate == null || !existingMandate.isPresent()) {
			throw new EntityNotFoundException("Cannot delete mandate with id='" + mandateId + "', as it doesn't exist");
		}
		mandateRepo.delete(existingMandate.get());
		mandateRepo.flush();
	}
}
package com.mazeed.vro.api.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
	String[] ignoredPaths = new String[] { "/", "/app/**", "/api/**", "/lib/**", "/error", "/login", "/doLogut", "/home", "/pageNotFound", "/css/**",
			"/js/**", "/fonts/**", "/img/**", "/swagger-resources/**", "/swagger-ui.html", "/v2/api-docs", "/webjars/**" };

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers(ignoredPaths).permitAll().anyRequest().authenticated();
		//This bypasses the authorization checks for pre-flight OPTIONS requests
		http.cors();
	}
}
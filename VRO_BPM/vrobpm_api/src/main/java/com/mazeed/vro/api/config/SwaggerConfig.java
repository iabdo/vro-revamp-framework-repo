package com.mazeed.vro.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.mazeed.vro.api.controller")).paths(PathSelectors.any())
				.build().apiInfo(apiEndPointsInfo()).tags(new Tag("morasalatyrequests", "API for managing morasalaty requests"),
                        new Tag("mandates", "API for managing mandates"));
	}

	private ApiInfo apiEndPointsInfo() {
		return new ApiInfoBuilder().title("VRO REST API").description("VRO REST API")
				.contact(new Contact("Islam Abdo", "", "iabdo@mazeed.co"))
				.version("1.0").build();
	}
}
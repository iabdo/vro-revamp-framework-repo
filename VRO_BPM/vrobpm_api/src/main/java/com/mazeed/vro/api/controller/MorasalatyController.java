package com.mazeed.vro.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mazeed.vro.api.service.MorasalatyService;
import com.mazeed.vro.core.entity.MorasalatyRequest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(tags = { "morasalatyrequests" })
public class MorasalatyController {

	@Autowired
	MorasalatyService morasalatyService;

	@ApiOperation(value = "View a list of all/standing morasalaty requests", response = Iterable.class, produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 500, message = "Internal Error")})
	@RequestMapping(path = "/morasalatyrequests", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> listMorasalatyRequests(
			@ApiParam(name = "standing", value = "Boolean value to determine whether to retrieve all or standing requests", required = false) 
			@RequestParam("standing") boolean isStanding,
			@ApiParam(name = "Authorization", value = "The bearer token value", required = true) 
			@RequestHeader(name = "Authorization", required = true) String bearerToken) {
		List<MorasalatyRequest> morasalatyRequests = null;
		morasalatyRequests = morasalatyService.findAll(isStanding);
		return new ResponseEntity<>(morasalatyRequests, HttpStatus.OK);
	}

	@ApiOperation(value = "View the details of a specific morasalaty request by id", response = MorasalatyRequest.class, produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved morasalaty request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 500, message = "Internal Error")})
	@RequestMapping(path = "/morasalatyrequests/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getMorasalatyRequestById(@PathVariable String id,
		@ApiParam(name = "Authorization", value = "The bearer token value", required = true) 
	@RequestHeader(name = "Authorization", required = true) String bearerToken) {
		MorasalatyRequest request = morasalatyService.findById(id);
		return new ResponseEntity<>(request, HttpStatus.OK);
	}

	@ApiOperation(value = "Create a new morasalaty request", response = MorasalatyRequest.class, produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully created morasalaty request"),
			@ApiResponse(code = 401, message = "You are not authorized to create the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 500, message = "Internal Error")})
	@RequestMapping(path = "/morasalatyrequests", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> createMorasalatyRequest(@RequestBody MorasalatyRequest request,
		@ApiParam(name = "Authorization", value = "The bearer token value", required = true) 
	@RequestHeader(name = "Authorization", required = true) String bearerToken) {
		morasalatyService.create(request);

		return new ResponseEntity<>(request, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Update a specific morasalaty request by id", response = MorasalatyRequest.class, produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully updated morasalaty request"),
			@ApiResponse(code = 401, message = "You are not authorized to update the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 500, message = "Internal Error")})
	@RequestMapping(path = "/morasalatyrequests/{id}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<?> updateMorasalatyRequest(@PathVariable String id, @RequestBody MorasalatyRequest request,
		@ApiParam(name = "Authorization", value = "The bearer token value", required = true) 
	@RequestHeader(name = "Authorization", required = true) String bearerToken) {
		morasalatyService.update(id, request);

		return new ResponseEntity<>(request, HttpStatus.OK);
	}

	@ApiOperation(value = "Delete a specific morasalaty request by id", produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully deleted morasalaty request"),
			@ApiResponse(code = 401, message = "You are not authorized to delete the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 500, message = "Internal Error")})
	@RequestMapping(path = "/morasalatyrequests/{id}", method = RequestMethod.DELETE, produces = "application/json")
	public ResponseEntity<?> deleteMorasalatyRequest(@PathVariable String id,
		@ApiParam(name = "Authorization", value = "The bearer token value", required = true) 
	@RequestHeader(name = "Authorization", required = true) String bearerToken) {
		morasalatyService.delete(id);

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
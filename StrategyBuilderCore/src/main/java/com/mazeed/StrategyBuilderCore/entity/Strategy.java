package com.mazeed.StrategyBuilderCore.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "STRATEGY")
public class Strategy extends Block {
	private static final long serialVersionUID = 1L;

	@Column(name = "organization_name")
	private String orgName;

	@Column(name = "organization_shortname")
	private double orgShortName;

	@Column(name = "organization_logo_path")
	private Date orgLogoPath;

	@Column(name = "vision")
	private Date vision;

	@Column(name = "mission")
	private Date mission;

	@Column(name = "strategy_values")
	private Date strategyValues;

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public double getOrgShortName() {
		return orgShortName;
	}

	public void setOrgShortName(double orgShortName) {
		this.orgShortName = orgShortName;
	}

	public Date getOrgLogoPath() {
		return orgLogoPath;
	}

	public void setOrgLogoPath(Date orgLogoPath) {
		this.orgLogoPath = orgLogoPath;
	}

	public Date getVision() {
		return vision;
	}

	public void setVision(Date vision) {
		this.vision = vision;
	}

	public Date getMission() {
		return mission;
	}

	public void setMission(Date mission) {
		this.mission = mission;
	}

	public Date getStrategyValues() {
		return strategyValues;
	}

	public void setStrategyValues(Date strategyValues) {
		this.strategyValues = strategyValues;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}

package com.mazeed.StrategyBuilderCore.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "GROUP")
public class Group extends AbstractEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8367899056043121922L;

	@Column(name = "group_name")
	private String name;

	@Column(name = "group_description")
	private String description;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "group_users", joinColumns = @JoinColumn(name = "group_uuid", referencedColumnName = "uuid"), inverseJoinColumns = @JoinColumn(name = "user_uuid", referencedColumnName = "uuid"))
	private List<User> users;

	@ManyToMany(mappedBy = "groups")
	private List<Role> roles;

	@ManyToMany(mappedBy = "blockGroups")
	private List<Block> permittedBlocks;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public List<Block> getPermittedBlocks() {
		return permittedBlocks;
	}

	public void setPermittedBlocks(List<Block> permittedBlocks) {
		this.permittedBlocks = permittedBlocks;
	}
}

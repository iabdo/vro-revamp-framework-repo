package com.mazeed.StrategyBuilderCore.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "LEVEL_TYPE")
public class LevelType extends AbstractEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3954225970002935795L;
	
	@Column(name = "type")
	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}

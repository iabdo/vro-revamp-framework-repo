package com.mazeed.StrategyBuilderCore.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ENABLER")
public class Enabler extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2988725105357913069L;

	@Column(name = "description")
	private String description;
	
	@Column(name = "goal")
	private String goal;
	
	@Column(name = "impact")
	private String impact;
	
	@ManyToOne
    @JoinColumn(name = "block_uuid")
	private Block block;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getGoal() {
		return goal;
	}

	public void setGoal(String goal) {
		this.goal = goal;
	}

	public String getImpact() {
		return impact;
	}

	public void setImpact(String impact) {
		this.impact = impact;
	}

	public Block getBlock() {
		return block;
	}

	public void setBlock(Block block) {
		this.block = block;
	}
}

package com.mazeed.StrategyBuilderCore.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "BLOCK")
@Inheritance(strategy = InheritanceType.JOINED)
public class Block extends AbstractEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6182573087640292776L;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;

	@Column(name = "progress")
	private double progress;

	@Column(name = "start_date")
	private Date startDate;

	@Column(name = "end_date")
	private Date endDate;

	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "parent_block_uuid", referencedColumnName = "uuid")
	private Block parentBlock;

	@OneToMany(mappedBy = "parentBlock")
	private List<Block> childBlocks;

	@OneToMany(mappedBy = "block", cascade = CascadeType.ALL)
	private List<CustomProperty> customProperties;

	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "owner_user_uuid", referencedColumnName = "uuid")
	private User ownerUser;

	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "level_type_uuid", referencedColumnName = "uuid")
	private LevelType levelType;

	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "priority_uuid", referencedColumnName = "uuid")
	private Priority priority;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "block_focus_areas", joinColumns = @JoinColumn(name = "block_uuid", referencedColumnName = "uuid"), inverseJoinColumns = @JoinColumn(name = "focus_area_uuid", referencedColumnName = "uuid"))
	private List<FocusArea> focusAreas;

	@OneToMany(mappedBy = "block", cascade = CascadeType.ALL)
	private List<Enabler> enablers;

	@OneToMany(mappedBy = "block", cascade = CascadeType.ALL)
	private List<Risk> risks;

	@OneToMany(mappedBy = "block", cascade = CascadeType.ALL)
	private List<SupportingDocument> supportingDocuments;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "block_users", joinColumns = @JoinColumn(name = "block_uuid", referencedColumnName = "uuid"), inverseJoinColumns = @JoinColumn(name = "user_uuid", referencedColumnName = "uuid"))
	private List<User> blockUsers;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "block_groups", joinColumns = @JoinColumn(name = "block_uuid", referencedColumnName = "uuid"), inverseJoinColumns = @JoinColumn(name = "group_uuid", referencedColumnName = "uuid"))
	private List<User> blockGroups;

	public Block() {
	}
	
	public Block(String name, String description) {
		this.name = name;
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getProgress() {
		return progress;
	}

	public void setProgress(double progress) {
		this.progress = progress;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Block getParentBlock() {
		return parentBlock;
	}

	public void setParentBlock(Block parentBlock) {
		this.parentBlock = parentBlock;
	}

	public List<Block> getChildBlocks() {
		return childBlocks;
	}

	public void setChildBlocks(List<Block> childBlocks) {
		this.childBlocks = childBlocks;
	}

	public List<CustomProperty> getCustomProperties() {
		return customProperties;
	}

	public void setCustomProperties(List<CustomProperty> customProperties) {
		this.customProperties = customProperties;
	}

	public User getOwnerUser() {
		return ownerUser;
	}

	public void setOwnerUser(User ownerUser) {
		this.ownerUser = ownerUser;
	}

	public LevelType getLevelType() {
		return levelType;
	}

	public void setLevelType(LevelType levelType) {
		this.levelType = levelType;
	}

	public Priority getPriority() {
		return priority;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	public List<FocusArea> getFocusAreas() {
		return focusAreas;
	}

	public void setFocusAreas(List<FocusArea> focusAreas) {
		this.focusAreas = focusAreas;
	}

	public List<Enabler> getEnablers() {
		return enablers;
	}

	public void setEnablers(List<Enabler> enablers) {
		this.enablers = enablers;
	}

	public List<Risk> getRisks() {
		return risks;
	}

	public void setRisks(List<Risk> risks) {
		this.risks = risks;
	}

	public List<SupportingDocument> getSupportingDocuments() {
		return supportingDocuments;
	}

	public void setSupportingDocuments(List<SupportingDocument> supportingDocuments) {
		this.supportingDocuments = supportingDocuments;
	}

	public List<User> getBlockUsers() {
		return blockUsers;
	}

	public void setBlockUsers(List<User> blockUsers) {
		this.blockUsers = blockUsers;
	}

	public List<User> getBlockGroups() {
		return blockGroups;
	}

	public void setBlockGroups(List<User> blockGroups) {
		this.blockGroups = blockGroups;
	}
}

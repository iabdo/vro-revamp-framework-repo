package com.mazeed.StrategyBuilderCore.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "KPI_INSTANCE_DATA_VARIABLES")
public class KPIInstanceDataVariable {
	@EmbeddedId
	private KPIInstanceDataVariableKey id;

	@ManyToOne
	@MapsId("kpi_instance_data_uuid")
	@JoinColumn(name = "kpi_instance_data_uuid", referencedColumnName = "uuid")
	private KPIInstanceData kpiInstanceData;

	@ManyToOne
	@MapsId("variable_uuid")
	@JoinColumn(name = "variable_uuid", referencedColumnName = "uuid")
	private Variable variable;

	@Column(name = "variable_value")
	private double variableValue;

	public KPIInstanceDataVariableKey getId() {
		return id;
	}

	public void setId(KPIInstanceDataVariableKey id) {
		this.id = id;
	}

	public KPIInstanceData getKpiInstanceData() {
		return kpiInstanceData;
	}

	public void setKpiInstanceData(KPIInstanceData kpiInstanceData) {
		this.kpiInstanceData = kpiInstanceData;
	}

	public Variable getVariable() {
		return variable;
	}

	public void setVariable(Variable variable) {
		this.variable = variable;
	}

	public double getVariableValue() {
		return variableValue;
	}

	public void setVariableValue(double variableValue) {
		this.variableValue = variableValue;
	}
}

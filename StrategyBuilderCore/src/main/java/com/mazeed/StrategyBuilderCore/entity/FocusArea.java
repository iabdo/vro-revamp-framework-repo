package com.mazeed.StrategyBuilderCore.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "FOCUS_AREA")
public class FocusArea extends AbstractEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6672225942286744063L;

	@Column(name = "title")
	private String title;

	@Column(name = "description")
	private String description;

	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "parent_focus_area_uuid", referencedColumnName = "uuid")
	private FocusArea parentFocusArea;

	@OneToMany(mappedBy = "parentFocusArea")
	private List<FocusArea> childFocusAreas;
	
	@ManyToMany(mappedBy = "focusAreas")
	private List<Block> blocks;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public FocusArea getParentFocusArea() {
		return parentFocusArea;
	}

	public void setParentFocusArea(FocusArea parentFocusArea) {
		this.parentFocusArea = parentFocusArea;
	}

	public List<FocusArea> getChildFocusAreas() {
		return childFocusAreas;
	}

	public void setChildFocusAreas(List<FocusArea> childFocusAreas) {
		this.childFocusAreas = childFocusAreas;
	}

	public List<Block> getBlocks() {
		return blocks;
	}

	public void setBlocks(List<Block> blocks) {
		this.blocks = blocks;
	}
}

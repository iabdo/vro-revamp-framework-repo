package com.mazeed.StrategyBuilderCore.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ROLE")
public class Role extends AbstractEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6097138727823067334L;

	@Column(name = "role_name")
	private String name;
	
	@Column(name = "role_description")
	private String description;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "group_roles", joinColumns = @JoinColumn(name = "role_uuid", referencedColumnName = "uuid"), inverseJoinColumns = @JoinColumn(name = "group_uuid", referencedColumnName = "uuid"))
	private List<Group> groups;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Group> getGroups() {
		return groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}
}

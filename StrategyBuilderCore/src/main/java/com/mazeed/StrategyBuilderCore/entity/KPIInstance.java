package com.mazeed.StrategyBuilderCore.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "KPI_INSTANCE")
public class KPIInstance extends KPIDefinition {

	/**
	 * 
	 */
	private static final long serialVersionUID = 509294694718929334L;

	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "cycle_type_uuid", referencedColumnName = "uuid")
	private CycleType cycleType;

	@Column(name = "target_value")
	private double targetValue;

	@Column(name = "current_value")
	private double currentValue;
	
	@OneToMany(mappedBy = "kpiInstance", cascade = CascadeType.ALL)
	private List<KPIInstanceData> kpiInstanceDataRecords;

	public CycleType getCycleType() {
		return cycleType;
	}

	public void setCycleType(CycleType cycleType) {
		this.cycleType = cycleType;
	}

	public double getTargetValue() {
		return targetValue;
	}

	public void setTargetValue(double targetValue) {
		this.targetValue = targetValue;
	}

	public double getCurrentValue() {
		return currentValue;
	}

	public void setCurrentValue(double currentValue) {
		this.currentValue = currentValue;
	}
}

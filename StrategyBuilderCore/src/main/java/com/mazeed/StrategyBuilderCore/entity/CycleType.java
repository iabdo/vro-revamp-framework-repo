package com.mazeed.StrategyBuilderCore.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "CYCLE_TYPE")
public class CycleType extends AbstractEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5127810662602776561L;

	@Column(name = "type")
	private String type;
	
	@Column(name = "duration_unit")
	private String durationUnit;
	
	@Column(name = "duration")
	private double duration;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDurationUnit() {
		return durationUnit;
	}

	public void setDurationUnit(String durationUnit) {
		this.durationUnit = durationUnit;
	}

	public double getDuration() {
		return duration;
	}

	public void setDuration(double duration) {
		this.duration = duration;
	}
}

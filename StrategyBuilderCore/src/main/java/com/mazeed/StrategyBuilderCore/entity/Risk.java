package com.mazeed.StrategyBuilderCore.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "RISK")
public class Risk extends AbstractEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5142206430723386117L;

	@Column(name = "priority")
	private String description;

	@ManyToOne
	@JoinColumn(name = "block_uuid")
	private Block block;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Block getBlock() {
		return block;
	}

	public void setBlock(Block block) {
		this.block = block;
	}
}

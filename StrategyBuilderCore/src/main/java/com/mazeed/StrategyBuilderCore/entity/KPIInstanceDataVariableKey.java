package com.mazeed.StrategyBuilderCore.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class KPIInstanceDataVariableKey implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7556001705328358825L;

	@Column(name = "kpi_instance_data_uuid")
	private String kpiInstanceDataId;

	@Column(name = "variable_uuid")
	private String variableId;

	public String getKpiInstanceDataId() {
		return kpiInstanceDataId;
	}

	public void setKpiInstanceDataId(String kpiInstanceDataId) {
		this.kpiInstanceDataId = kpiInstanceDataId;
	}

	public String getVariableId() {
		return variableId;
	}

	public void setVariableId(String variableId) {
		this.variableId = variableId;
	}
}

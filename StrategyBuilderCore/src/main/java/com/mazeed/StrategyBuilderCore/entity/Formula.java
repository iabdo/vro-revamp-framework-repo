package com.mazeed.StrategyBuilderCore.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "FORMULA")
public class Formula extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2242957674220692867L;

	@Column(name = "description")
	private String description;

	@Column(name = "formula_expression")
	private String formulaExpression;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "formula_variables", joinColumns = @JoinColumn(name = "formula_uuid", referencedColumnName = "uuid"), inverseJoinColumns = @JoinColumn(name = "variable_uuid", referencedColumnName = "uuid"))
	private List<Variable> formulaVariables;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFormulaExpression() {
		return formulaExpression;
	}

	public void setFormulaExpression(String formulaExpression) {
		this.formulaExpression = formulaExpression;
	}

	public List<Variable> getFormulaVariables() {
		return formulaVariables;
	}

	public void setFormulaVariables(List<Variable> formulaVariables) {
		this.formulaVariables = formulaVariables;
	}
}

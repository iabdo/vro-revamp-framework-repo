package com.mazeed.StrategyBuilderCore.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "USER")
public class User extends AbstractEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4277269531557451041L;

	@Column(name = "type")
	private String type;

	@OneToMany(mappedBy = "ownerUser", cascade = CascadeType.ALL)
	private List<Block> ownedBlocks;

	@ManyToMany(mappedBy = "blockUsers")
	private List<Block> permittedBlocks;

	@ManyToMany(mappedBy = "users")
	private List<Group> groups;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Block> getOwnedBlocks() {
		return ownedBlocks;
	}

	public void setOwnedBlocks(List<Block> ownedBlocks) {
		this.ownedBlocks = ownedBlocks;
	}

	public List<Block> getPermittedBlocks() {
		return permittedBlocks;
	}

	public void setPermittedBlocks(List<Block> permittedBlocks) {
		this.permittedBlocks = permittedBlocks;
	}

	public List<Group> getGroups() {
		return groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}
}

package com.mazeed.StrategyBuilderCore.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "POLARITY_TYPE")
public class PolarityType extends AbstractEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4302951122887397340L;

	@Column(name = "type")
	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}

package com.mazeed.StrategyBuilderCore.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "BLOCK_SUPPORTING_DOCUMENTS")
public class SupportingDocument extends AbstractEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6924566248997037264L;

	@Column(name = "document_name")
	private String documentName;

	@Column(name = "document_path")
	private String documentPath;

	@ManyToOne
	@JoinColumn(name = "block_uuid")
	private Block block;

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getDocumentPath() {
		return documentPath;
	}

	public void setDocumentPath(String documentPath) {
		this.documentPath = documentPath;
	}

	public Block getBlock() {
		return block;
	}

	public void setBlock(Block block) {
		this.block = block;
	}
}

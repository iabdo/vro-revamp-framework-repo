package com.mazeed.StrategyBuilderCore.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "KPI_INSTANCE_DATA")
public class KPIInstanceData extends AbstractEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5348178057748683021L;

	@Column(name = "cycle_date")
	private Date cycleDate;

	@Column(name = "cycle_current_value")
	private double cycleCurrentValue;

	@Column(name = "cycle_target_value")
	private double cycleTargetValue;

	@ManyToOne
	@JoinColumn(name = "kpi_instance_uuid")
	private KPIInstance kpiInstance;

	@OneToMany(mappedBy = "kpiInstanceData")
	List<KPIInstanceDataVariable> instanceDataVariables;

	public Date getCycleDate() {
		return cycleDate;
	}

	public void setCycleDate(Date cycleDate) {
		this.cycleDate = cycleDate;
	}

	public double getCycleCurrentValue() {
		return cycleCurrentValue;
	}

	public void setCycleCurrentValue(double cycleCurrentValue) {
		this.cycleCurrentValue = cycleCurrentValue;
	}

	public double getCycleTargetValue() {
		return cycleTargetValue;
	}

	public void setCycleTargetValue(double cycleTargetValue) {
		this.cycleTargetValue = cycleTargetValue;
	}

	public KPIInstance getKpiInstance() {
		return kpiInstance;
	}

	public void setKpiInstance(KPIInstance kpiInstance) {
		this.kpiInstance = kpiInstance;
	}

	public List<KPIInstanceDataVariable> getInstanceDataVariables() {
		return instanceDataVariables;
	}

	public void setInstanceDataVariables(List<KPIInstanceDataVariable> instanceDataVariables) {
		this.instanceDataVariables = instanceDataVariables;
	}
}

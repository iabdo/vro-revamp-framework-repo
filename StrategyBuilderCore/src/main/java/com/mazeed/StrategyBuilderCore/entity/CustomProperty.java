package com.mazeed.StrategyBuilderCore.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "CUSTOM_PROPERTY")
public class CustomProperty extends AbstractEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3198370577722066901L;

	@Column(name = "property_key")
	private String propertyKey;
	
	@Column(name = "property_value")
	private String propertyValue;
	
	@ManyToOne
    @JoinColumn(name = "block_uuid")
	private Block block;

	public String getPropertyKey() {
		return propertyKey;
	}

	public void setPropertyKey(String propertyKey) {
		this.propertyKey = propertyKey;
	}

	public String getPropertyValue() {
		return propertyValue;
	}

	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}

	public Block getBlock() {
		return block;
	}

	public void setBlock(Block block) {
		this.block = block;
	}
}

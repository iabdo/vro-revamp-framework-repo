package com.mazeed.StrategyBuilderCore.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "PRIORITY")
public class Priority extends AbstractEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5957186628005026041L;

	@Column(name = "priority")
	private String priority;

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}
}

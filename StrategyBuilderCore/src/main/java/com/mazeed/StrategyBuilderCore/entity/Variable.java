package com.mazeed.StrategyBuilderCore.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "VARIABLE")
public class Variable extends AbstractEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1094627072896724815L;

	@Column(name = "variable_name")
	private String variableName;

	@Column(name = "validation_regex")
	private String validationRegex;

	@Column(name = "default_value")
	private double defaultValue;

	@ManyToMany(mappedBy = "formulaVariables")
	private List<Formula> formulas;

	public String getVariableName() {
		return variableName;
	}

	public void setVariableName(String variableName) {
		this.variableName = variableName;
	}

	public String getValidationRegex() {
		return validationRegex;
	}

	public void setValidationRegex(String validationRegex) {
		this.validationRegex = validationRegex;
	}

	public double getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(double defaultValue) {
		this.defaultValue = defaultValue;
	}

	public List<Formula> getFormulas() {
		return formulas;
	}

	public void setFormulas(List<Formula> formulas) {
		this.formulas = formulas;
	}
}

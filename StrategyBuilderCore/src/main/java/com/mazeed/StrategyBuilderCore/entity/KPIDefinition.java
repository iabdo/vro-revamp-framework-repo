package com.mazeed.StrategyBuilderCore.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "KPI_DEFINITION")
@Inheritance(strategy = InheritanceType.JOINED)
public class KPIDefinition extends AbstractEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8716715839961628033L;

	@Column(name = "display_name")
	private String displayName;

	@Column(name = "description")
	private String description;

	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "formula_uuid", referencedColumnName = "uuid")
	private Formula formula;

	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "polarity_type_uuid", referencedColumnName = "uuid")
	private PolarityType polarityType;

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Formula getFormula() {
		return formula;
	}

	public void setFormula(Formula formula) {
		this.formula = formula;
	}

	public PolarityType getPolarityType() {
		return polarityType;
	}

	public void setPolarityType(PolarityType polarityType) {
		this.polarityType = polarityType;
	}
}

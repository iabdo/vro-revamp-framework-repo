package com.mazeed.StrategyBuilderCore.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.mazeed.StrategyBuilderCore.entity.KPIInstance;

@RepositoryRestResource(collectionResourceRel = "kpiinstance", path = "kpiinstance")
public interface KPIInstanceRepository extends CrudRepository<KPIInstance, String> {

}
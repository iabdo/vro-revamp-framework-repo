package com.mazeed.StrategyBuilderCore.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.mazeed.StrategyBuilderCore.entity.KPIDefinition;

@RepositoryRestResource(collectionResourceRel = "kpidef", path = "kpidef")
public interface KPIDefinitionRepository extends CrudRepository<KPIDefinition, String> {

}
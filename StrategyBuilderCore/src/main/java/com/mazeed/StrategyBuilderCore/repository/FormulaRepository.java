package com.mazeed.StrategyBuilderCore.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.mazeed.StrategyBuilderCore.entity.Formula;

@RepositoryRestResource(collectionResourceRel = "formulas", path = "formulas")
public interface FormulaRepository extends CrudRepository<Formula, String> {

}
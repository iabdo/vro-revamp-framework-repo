package com.mazeed.StrategyBuilderCore.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.mazeed.StrategyBuilderCore.entity.Strategy;

@RepositoryRestResource(collectionResourceRel = "strategies", path = "strategies")
public interface StrategyRepository extends CrudRepository<Strategy, String> {

}

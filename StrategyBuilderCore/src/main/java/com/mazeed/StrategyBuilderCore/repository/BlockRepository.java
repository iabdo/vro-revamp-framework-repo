package com.mazeed.StrategyBuilderCore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.mazeed.StrategyBuilderCore.entity.Block;

@RepositoryRestResource(collectionResourceRel = "blocks", path = "blocks")
public interface BlockRepository extends JpaRepository<Block, String> {
	
	List<Block> findByNameStartsWithIgnoreCase(String name); 
}

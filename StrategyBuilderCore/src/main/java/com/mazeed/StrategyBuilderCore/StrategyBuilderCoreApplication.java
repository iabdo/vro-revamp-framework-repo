package com.mazeed.StrategyBuilderCore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StrategyBuilderCoreApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(StrategyBuilderCoreApplication.class, args);
	}

}

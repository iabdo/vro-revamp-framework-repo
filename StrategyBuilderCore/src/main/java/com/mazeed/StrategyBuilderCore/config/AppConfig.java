package com.mazeed.StrategyBuilderCore.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@Configuration
@EnableJpaAuditing
@ComponentScan("com.mazeed.StrategyBuilderCore")
public class AppConfig {

}

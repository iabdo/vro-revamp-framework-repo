package com.mazeed.StrategyBuilderCore.service;

import org.springframework.stereotype.Service;

import com.mazeed.StrategyBuilderCore.entity.Formula;

@Service
public interface FormulaService {
	public boolean validateFormulaExpression(Formula formula);
}

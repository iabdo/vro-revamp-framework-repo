package com.mazeed.StrategyBuilderCore.service.impl;

import com.mazeed.StrategyBuilderCore.entity.Formula;
import com.mazeed.StrategyBuilderCore.service.FormulaService;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import net.objecthunter.exp4j.ValidationResult;

public class FormulaServiceImpl implements FormulaService {

	@Override
	public boolean validateFormulaExpression(Formula formula) {
		// TODO:
		// Uncomment the following code to activate actual formula validation
//		Set<String> formulaVariables = new HashSet<String>();
//		
//		if (formula != null && formula.getFormulaVariables() != null) {
//			for (int i = 0; i < formula.getFormulaVariables().size(); i++) {
//				formulaVariables.add(formula.getFormulaVariables().get(i).getVariableName());
//			}
//		}
//		
//		Expression e = new ExpressionBuilder(formula.getFormulaExpression()).variables(formulaVariables).build();
//		ValidationResult res = e.validate(false);
//		return res.isValid();
		// TODO:
		// comment the following code to disable testing the formula validation
		Expression e = new ExpressionBuilder("[x] + [y]").variable("x").variable("y").build();
		ValidationResult res = e.validate(false);

		return res.isValid();
	}

}

package com.mazeed.StrategyBuilderCore.view;

import org.springframework.beans.factory.annotation.Autowired;

import com.mazeed.StrategyBuilderCore.entity.Block;
import com.mazeed.StrategyBuilderCore.repository.BlockRepository;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyNotifier;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.PropertyId;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

@SpringComponent
@UIScope
public class BlockEditor extends VerticalLayout implements KeyNotifier {
	private final BlockRepository repository;

	/**
	 * The currently edited block
	 */
	private Block block;

	/* Fields to edit properties in Block entity */
	//@PropertyId("name")
	//TextField name = new TextField("name");
	@PropertyId("description")
	TextField description = new TextField("description");

	/* Action buttons */
	// TODO why more code?
	Button save = new Button("Save", VaadinIcon.CHECK.create());
	Button cancel = new Button("Cancel");
	Button delete = new Button("Delete", VaadinIcon.TRASH.create());
	HorizontalLayout actions = new HorizontalLayout(save, cancel, delete);

	Binder<Block> binder = new Binder<>(Block.class);
	private ChangeHandler changeHandler;

	@Autowired
	public BlockEditor(BlockRepository repository) {
		this.repository = repository;

		add(/*name,*/description, actions);

		// bind using naming convention
		binder.bindInstanceFields(this);

		// Configure and style components
		setSpacing(true);

		save.getElement().getThemeList().add("primary");
		delete.getElement().getThemeList().add("error");

		addKeyPressListener(Key.ENTER, e -> save());

		// wire action buttons to save, delete and reset
		save.addClickListener(e -> save());
		delete.addClickListener(e -> delete());
		cancel.addClickListener(e -> editBlock(block));
		setVisible(false);
	}

	void delete() {
		repository.delete(block);
		changeHandler.onChange();
	}

	void save() {
		repository.save(block);
		changeHandler.onChange();
	}

	public interface ChangeHandler {
		void onChange();
	}

	public final void editBlock(Block c) {
		if (c == null) {
			setVisible(false);
			return;
		}
		final boolean persisted = c.getId() != null;
		if (persisted) {
			// Find fresh entity for editing
			block = repository.findById(c.getId()).get();
		} else {
			block = c;
		}
		cancel.setVisible(persisted);

		// Bind customer properties to similarly named fields
		// Could also use annotation or "manual binding" or programmatically
		// moving values from fields to entities before saving
		binder.setBean(block);

		setVisible(true);

		// Focus first name initially
		//name.focus();
	}

	public void setChangeHandler(ChangeHandler h) {
		// ChangeHandler is notified when either save or delete
		// is clicked
		changeHandler = h;
	}
}

package com.mazeed.StrategyBuilderCore.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.mazeed.StrategyBuilderCore.entity.Block;
import com.mazeed.StrategyBuilderCore.repository.BlockRepository;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;

@Route(value = "sb")
public class MainView extends VerticalLayout {
	private BlockRepository repo;
	private BlockEditor editor;

	Grid<Block> grid;

	TextField filter;

	private Button addNewBtn;

	@Autowired
	public MainView(BlockRepository repo, BlockEditor editor) {
		this.repo = repo;
		this.editor = editor;
		this.grid = new Grid<>(Block.class);
		this.filter = new TextField();
		this.addNewBtn = new Button("New block", VaadinIcon.PLUS.create());

		// build layout
		HorizontalLayout actions = new HorizontalLayout(filter, addNewBtn);
		add(actions, grid, editor);

		grid.setHeight("300px");
		grid.setColumns("id", /*"name",*/ "description");
		grid.getColumnByKey("id").setWidth("50px").setFlexGrow(0);

		filter.setPlaceholder("Filter by name");

		// Hook logic to components

		// Replace listing with filtered content when user changes filter
		filter.setValueChangeMode(ValueChangeMode.EAGER);
		filter.addValueChangeListener(e -> listBlocks(e.getValue()));

		// Connect selected Block to editor or hide if none is selected
		grid.asSingleSelect().addValueChangeListener(e -> {
			editor.editBlock(e.getValue());
		});

		// Instantiate and edit new Block the new button is clicked
		addNewBtn.addClickListener(e -> editor.editBlock(new Block("", "")));

		// Listen changes made by the editor, refresh data from backend
		editor.setChangeHandler(() -> {
			editor.setVisible(false);
			listBlocks(filter.getValue());
		});

		// Initialize listing
		listBlocks(null);
	}

	// tag::listCustomers[]
	void listBlocks(String filterText) {
		if (StringUtils.isEmpty(filterText)) {
			grid.setItems(repo.findAll());
		} else {
			grid.setItems(repo.findByNameStartsWithIgnoreCase(filterText));
		}
	}
	// end::listCustomers[]

}

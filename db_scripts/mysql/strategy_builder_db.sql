/* DROP TABLES */
DROP TABLE IF EXISTS CUSTOM_PROPERTY;
DROP TABLE IF EXISTS RISK;
DROP TABLE IF EXISTS ENABLER;
DROP TABLE IF EXISTS BLOCK_SUPPORTING_DOCUMENTS;
DROP TABLE IF EXISTS BLOCK_FOCUS_AREAS;
DROP TABLE IF EXISTS BLOCK_USERS;
DROP TABLE IF EXISTS BLOCK_GROUPS;
DROP TABLE IF EXISTS FOCUS_AREA;
DROP TABLE IF EXISTS STRATEGY;
DROP TABLE IF EXISTS `BLOCK`;
DROP TABLE IF EXISTS FORMULA_VARIABLES;
DROP TABLE IF EXISTS KPI_INSTANCE_DATA_VARIABLES;
DROP TABLE IF EXISTS KPI_INSTANCE_DATA;
DROP TABLE IF EXISTS KPI_INSTANCE;
DROP TABLE IF EXISTS KPI_DEFINITION;
DROP TABLE IF EXISTS LEVEL_TYPE;
DROP TABLE IF EXISTS PRIORITY;
DROP TABLE IF EXISTS CYCLE_TYPE;
DROP TABLE IF EXISTS POLARITY_TYPE;
DROP TABLE IF EXISTS GROUP_ROLES;
DROP TABLE IF EXISTS GROUP_USERS;
DROP TABLE IF EXISTS `USER`;
DROP TABLE IF EXISTS `GROUP`;
DROP TABLE IF EXISTS `ROLE`;
/*--------------------------------------------------------------*/
/* CREATE TABLES */
CREATE TABLE `USER` (uuid VARCHAR(36), 
type VARCHAR(50),

PRIMARY KEY (uuid));
/*--------------------------------------------------------------*/
CREATE TABLE `GROUP` (uuid VARCHAR(36), 
group_name VARCHAR(50),
group_description VARCHAR(200),

PRIMARY KEY (uuid));
/*--------------------------------------------------------------*/
CREATE TABLE `ROLE` (uuid VARCHAR(36), 
role_name VARCHAR(50),
role_description VARCHAR(200),

PRIMARY KEY (uuid));
/*--------------------------------------------------------------*/
CREATE TABLE GROUP_ROLES (group_uuid VARCHAR(36), 
role_uuid VARCHAR(36),

PRIMARY KEY (group_uuid, role_uuid),
FOREIGN KEY (group_uuid) REFERENCES `GROUP`(uuid),
FOREIGN KEY (role_uuid) REFERENCES `ROLE`(uuid));
/*--------------------------------------------------------------*/
CREATE TABLE GROUP_USERS (group_uuid VARCHAR(36), 
user_uuid VARCHAR(36),

PRIMARY KEY (group_uuid, user_uuid),
FOREIGN KEY (group_uuid) REFERENCES `GROUP`(uuid),
FOREIGN KEY (user_uuid) REFERENCES `USER`(uuid));
/*--------------------------------------------------------------*/
CREATE TABLE LEVEL_TYPE (uuid VARCHAR(36), 
type VARCHAR(50),

PRIMARY KEY (uuid));
/*--------------------------------------------------------------*/
CREATE TABLE PRIORITY (uuid VARCHAR(36), 
priority VARCHAR(50),

PRIMARY KEY (uuid));
/*--------------------------------------------------------------*/
CREATE TABLE CYCLE_TYPE (uuid VARCHAR(36), 
type VARCHAR(50),
duration DECIMAL,
duration_unit VARCHAR(10),

PRIMARY KEY (uuid));
/*--------------------------------------------------------------*/
CREATE TABLE POLARITY_TYPE (uuid VARCHAR(36), 
type VARCHAR(50),

PRIMARY KEY (uuid));
/*--------------------------------------------------------------*/
CREATE TABLE FOCUS_AREA (uuid VARCHAR(36), 
parent_focus_area_uuid VARCHAR(36),
title VARCHAR(50),
description VARCHAR(300),

PRIMARY KEY (uuid),
FOREIGN KEY (parent_focus_area_uuid) REFERENCES FOCUS_AREA(uuid));
/*--------------------------------------------------------------*/
CREATE TABLE `BLOCK` (uuid VARCHAR(36), 
description VARCHAR(300),
progress DECIMAL(5,2),
start_date DATETIME,
end_date DATETIME,
parent_block_uuid VARCHAR(36),
owner_user_uuid VARCHAR(36),
level_type_uuid VARCHAR(36),
priority_uuid VARCHAR(36),

PRIMARY KEY (uuid),
FOREIGN KEY (parent_block_uuid) REFERENCES BLOCK(uuid),
FOREIGN KEY (owner_user_uuid) REFERENCES `USER`(uuid),
FOREIGN KEY (level_type_uuid) REFERENCES LEVEL_TYPE(uuid),
FOREIGN KEY (priority_uuid) REFERENCES PRIORITY(uuid));
/*--------------------------------------------------------------*/
CREATE TABLE STRATEGY (uuid VARCHAR(36), 
organization_name VARCHAR(50),
organization_shortname VARCHAR(50),
organization_logo_path VARCHAR(100),
vision VARCHAR(200),
mission VARCHAR(200),
strategy_values VARCHAR(500),

PRIMARY KEY (uuid),
FOREIGN KEY (uuid) REFERENCES BLOCK(uuid));
/*--------------------------------------------------------------*/
CREATE TABLE BLOCK_FOCUS_AREAS (block_uuid VARCHAR(36), 
focus_area_uuid VARCHAR(36),

PRIMARY KEY (block_uuid, focus_area_uuid),
FOREIGN KEY (block_uuid) REFERENCES BLOCK(uuid),
FOREIGN KEY (focus_area_uuid) REFERENCES FOCUS_AREA(uuid));
/*--------------------------------------------------------------*/
CREATE TABLE BLOCK_USERS (block_uuid VARCHAR(36), 
user_uuid VARCHAR(36),

PRIMARY KEY (block_uuid, user_uuid),
FOREIGN KEY (block_uuid) REFERENCES BLOCK(uuid),
FOREIGN KEY (user_uuid) REFERENCES `USER`(uuid));
/*--------------------------------------------------------------*/
CREATE TABLE BLOCK_GROUPS (block_uuid VARCHAR(36), 
group_uuid VARCHAR(36),

PRIMARY KEY (block_uuid, group_uuid),
FOREIGN KEY (block_uuid) REFERENCES BLOCK(uuid),
FOREIGN KEY (group_uuid) REFERENCES `GROUP`(uuid));
/*--------------------------------------------------------------*/
CREATE TABLE CUSTOM_PROPERTY (uuid VARCHAR(36), 
property_key VARCHAR(50),
property_value VARCHAR(300),
block_uuid VARCHAR(36),

PRIMARY KEY (uuid),
FOREIGN KEY (block_uuid) REFERENCES BLOCK(uuid));
/*--------------------------------------------------------------*/
CREATE TABLE RISK (uuid VARCHAR(36), 
description VARCHAR(300),
block_uuid VARCHAR(36),

PRIMARY KEY (uuid),
FOREIGN KEY (block_uuid) REFERENCES BLOCK(uuid));
/*--------------------------------------------------------------*/
CREATE TABLE ENABLER (uuid VARCHAR(36), 
description VARCHAR(300),
goal VARCHAR(100),
impact VARCHAR(300),
block_uuid VARCHAR(36),

PRIMARY KEY (uuid),
FOREIGN KEY (block_uuid) REFERENCES BLOCK(uuid));
/*--------------------------------------------------------------*/
CREATE TABLE BLOCK_SUPPORTING_DOCUMENTS (uuid VARCHAR(36), 
document_name VARCHAR(50),
document_path VARCHAR(100),
block_uuid VARCHAR(36),

PRIMARY KEY (uuid),
FOREIGN KEY (block_uuid) REFERENCES BLOCK(uuid));
/*--------------------------------------------------------------*/
DROP TABLE IF EXISTS FORMULA;
CREATE TABLE FORMULA (uuid VARCHAR(36), 
formula VARCHAR(150),
description VARCHAR(200),

PRIMARY KEY (uuid));
/*--------------------------------------------------------------*/
DROP TABLE IF EXISTS VARIABLE;
CREATE TABLE VARIABLE (uuid VARCHAR(36), 
variable_name VARCHAR(50), 
validation_regex VARCHAR(100),
default_value DECIMAL,

PRIMARY KEY (uuid),
UNIQUE KEY (variable_name));
/*--------------------------------------------------------------*/
CREATE TABLE FORMULA_VARIABLES (formula_uuid VARCHAR(36), 
variable_uuid VARCHAR(36),

PRIMARY KEY (formula_uuid, variable_uuid),
FOREIGN KEY (formula_uuid) REFERENCES FORMULA(uuid),
FOREIGN KEY (variable_uuid) REFERENCES VARIABLE(uuid));
/*--------------------------------------------------------------*/
CREATE TABLE KPI_DEFINITION (uuid VARCHAR(36), 
display_name VARCHAR(50),
description VARCHAR(300),
formula_uuid VARCHAR(36),
polarity_type_uuid VARCHAR(36),

PRIMARY KEY (uuid),
FOREIGN KEY (formula_uuid) REFERENCES FORMULA(uuid),
FOREIGN KEY (polarity_type_uuid) REFERENCES POLARITY_TYPE(uuid));
/*--------------------------------------------------------------*/
CREATE TABLE KPI_INSTANCE (uuid VARCHAR(36), 
cycle_type_uuid VARCHAR(36),
target_value DECIMAL,
current_value DECIMAL,

PRIMARY KEY (uuid),
FOREIGN KEY (uuid) REFERENCES KPI_DEFINITION(uuid),
FOREIGN KEY (cycle_type_uuid) REFERENCES CYCLE_TYPE(uuid));
/*--------------------------------------------------------------*/
CREATE TABLE KPI_INSTANCE_DATA (uuid VARCHAR(36), 
kpi_instance_uuid VARCHAR(36),
cycle_date DATETIME,
cycle_current_value DECIMAL,
cycle_target_value DECIMAL,

PRIMARY KEY (uuid),
FOREIGN KEY (kpi_instance_uuid) REFERENCES KPI_INSTANCE(uuid));
/*--------------------------------------------------------------*/
CREATE TABLE KPI_INSTANCE_DATA_VARIABLES (kpi_instance_data_uuid VARCHAR(36),
variable_uuid VARCHAR(36),
variable_value DECIMAL,

PRIMARY KEY (kpi_instance_data_uuid, variable_uuid),
FOREIGN KEY (kpi_instance_data_uuid) REFERENCES KPI_INSTANCE_DATA(uuid),
FOREIGN KEY (variable_uuid) REFERENCES VARIABLE(uuid));
/*--------------------------------------------------------------*/